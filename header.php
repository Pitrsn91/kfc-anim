<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=yes">
	<title><?php echo is_404() ? 'Stránka nenalezena' : get_the_title(); ?> | <?= CD\ThemeConfig::getCompanyName(); ?></title>
	<meta name="author" content="LMC s.r.o. | www.lmc.eu (c) <?php echo date( 'Y' ); ?>">
	<?php
	getComponent( 'meta', array(), 'robots' );
	getComponent( 'meta', array(), 'description' );
	getComponent( 'favicons' ); //TODO doplnit favikony
	getComponent( 'share-icons' ); //TODO upravit obrazky
	//TODO nastavit theme-config.php
	?>
	<!-- #CSS -->
	<link rel="stylesheet" type="text/css" href="//presentation.lmc.cz/cse_templates/css/v-2-0-3/lmc-styles.css<?= \CD\GlobalVersion::get();?>">
	<link rel="stylesheet" type="text/css" href="<?php echo getThemeUrl(); ?>css/style.min.css<?php v(); ?>">
	<!-- #JS -->
	<script src="//presentation.lmc.cz/libs/jquery/jquery.3.2.1.js<?= \CD\GlobalVersion::get();?>"></script>
	<script>
		var cse$ = $.noConflict(),
			$ = cse$;
	</script>
	<?php wp_head(); ?>
</head>
<body class="no-js">
	<?php
	echo function_exists( 'getCSEHTMLBodyTop' ) ? getCSEHTMLBodyTop() : '';
	getComponent( 'no-js' );
	getComponent( 'deprecated-browser' );
	?>
