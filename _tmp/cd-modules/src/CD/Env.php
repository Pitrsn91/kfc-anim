<?php

namespace CD;

use SimpleXMLElement;

/**
 * Class Env
 *
 * Finds out what's current environment. Gets lmcenv.xml file on the server and gets current environment.
 *
 * @package CD
 */
class Env {

	/** Uri to lmcenv.xml file */
	const ENV_FILE_URI = '/etc/lmcenv.xml';

	/** @var array Possible env tier names */
	private static $envNames = [ 'local', 'dev', 'devel', 'deploy', 'prod' ];

	/** @var int Tier name id */
	private static $envNameId;

	/**
	 * Returns env tier name
	 * @return string
	 */
	public static function getEnvName() {
		self::init();

		return self::$envNames[ self::$envNameId ];
	}

	/**
	 * Returns if current env is DEV or DEVEL
	 * @return bool
	 */
	public static function isDev() {
		self::init();

		return self::$envNameId === 1 || self::$envNameId === 2;
	}

	/**
	 * Returns if current env is outside LMC server infrastructure. It's probably "local" env
	 * @return bool
	 */
	public static function isLocal() {
		self::init();

		return self::$envNameId === 0;
	}

	/**
	 * Reads lmcenv.xml file and finds out current env
	 */
	private static function init() {
		if ( is_null( self::$envNameId ) ) {
			if ( @is_readable( self::ENV_FILE_URI ) ) {
				$content         = new SimpleXMLElement( file_get_contents( self::ENV_FILE_URI ) );
				self::$envNameId = array_search( $content->attributes()['tier'], self::$envNames );
			} else {
				self::$envNameId = 0;
			}
		}
	}

}
