<?php

namespace CD;

/**
 * Class ThemeConfigBase
 *
 * Base class for ThemeConfig. It stores variables for template Settings as siteVersion, company name. etc. and public functions
 * Functions in this class are shared via ThemeConfig class across all websites
 *
 * @package CD
 */

class ThemeConfigBase {
	/** @var string $siteVersion */
	static private $siteVersion = "";
	/** @var string $companyName */
	static protected $companyName = "";
	/** @var array $detailPagesIds */
	static protected $detailPageIds = array();
	/** @var array $noIndexPagesIds */
	static protected $noIndexPageIds = array();
	/** @var string $siteKeywords */
	static protected $siteKeywords = "";

	/**
	 *  Method check where it was called and defines right path to txt file and initializes @var $siteVersion;
	 *  Txt file contains string with currentVersion
	 *
	 */
	static private function loadSiteVersion(){
		$backtrace = debug_backtrace();
		$templateRoot = (isset($backtrace["1"]) && array_key_exists("file",$backtrace["1"])) ? dirname($backtrace[1]['file']) : "";
		$versionFilePath = $templateRoot . "/time-define.txt";
		if(is_readable($versionFilePath)){
			self::$siteVersion = trim(file_get_contents($versionFilePath));
		}
	}

	/**
	 *  Basic init functions. All config functions should be called in this function.
	 *  Should be called only once.
	 *
	 */
	static public function init(){
		self::loadSiteVersion();
	}

	/**
	 * Returns project's siteVersion
	 *
	 * @return string.
	 */
	static public function getSiteVersion(){
		return self::$siteVersion;
	}

	/**
	 * Returns project's company name which is defined in child class ThemeConfig.
	 * Company name is used in <title> or <meta description>
	 *
	 * @return string.
	 */
	static public function getCompanyName(){
		return static::$companyName;
	}

	/**
	 * Returns project's detail page ids which is defined in child class ThemeConfig.
	 * This variable contain position detail page IDs from wordpress.
	 *
	 *
	 * @return array.
	 */
	static public function getDetailPagesIds(){
		return static::$detailPageIds;
	}

	/**
	 * Returns project's noindex Pages ids which is defined in child class ThemeConfig.
	 * This variable contain position detail, forms page IDs from wordpress.
	 *
	 * @return array.
	 */
	static public function getNoIndexPagesIds(){
		return static::$noIndexPageIds;
	}

	/**
	 * Returns project's keywords which is used in meta tag <meta keywords>
	 *
	 * @return string.
	 */
	static public function getSiteKeywords(){
		return static::$siteKeywords;
	}
}
