<?php

namespace CD;

/**
 * Class GlobalVersion
 *
 * Usable for versioning media files, styles, scripts and other external files linked to HTML web page with global
 * version shared between all projects. Helps browsers to cache resources right.
 *
 * This class reads version from txt file defined inside GLOBAL_VERSION_PATH constant.
 *
 * @package CD
 */
class GlobalVersion extends Version {

	/** @const string Absolute path to file with global version */
	const GLOBAL_VERSION_PATH = '/srv/www/cse-wp/designVersion.txt';

	/** @var string $currentVersion */
	protected static $currentVersion = '';
	/** @var string $defaultVersion */
	protected static $defaultVersion = '1.0.0';

	/**
	 * Initializes global version. Version is read from global version file, that is shared between all projects and
	 * installed on all environments
	 * (dev, deploy, prod...). Default version is used when file is not available or cannot be read.
	 *
	 * Warnings in is_readable are muted, because when there are restricted paths set in open_basedir php
	 * configuration,
	 * functions like is_dir, is_readable, file_exits etc. can print warnings that cannot be caught.
	 */
	protected static function init() {
		if ( empty( static::$currentVersion ) ) {
			if ( @is_readable( static::GLOBAL_VERSION_PATH ) ) {
				$globalVersion          = trim( file_get_contents( static::GLOBAL_VERSION_PATH ) );
				static::$currentVersion = empty( $globalVersion ) ? static::$defaultVersion : $globalVersion;
			} else if ( ! Env::isLocal() ) {
				static::$currentVersion = static::$defaultVersion;
			} else {
				static::$currentVersion = date( "His" );
			}
		}
	}
}
