<?php

namespace CD;

/**
 * Class Lang
 * <br>
 * Handles translations of component texts. Can be called in any component, reads file "lang.json" with translations and
 * returns them by key.
 * <br>
 * <b>File example:</b> {"text_key": {"cs": "Preklad", "en": "Translation"}}
 * <br>
 * <br>
 * Language is chosen automatically from WP or by get param "lang=<language (en|cs|sk|...)>".
 * <br>
 * <b>How to set WP lang:</b> global $locale; $locale = 'en';
 */
class Lang {
	const LANG_FILE_NAME = 'lang.json';
	private static $defaultModulesPath;

	/**
	 * @var string
	 */
	private $defaultLang;
	/**
	 * @var string
	 */
	private static $actualLang;
	/**
	 * @var array
	 */
	private $texts = array();

	/**
	 * Lang constructor.
	 *
	 * @param string $componentName Optional. If set, it overrides automatic component name.
	 */
	public function __construct( $componentName = '' ) {
		self::$defaultModulesPath = __DIR__ . '/../../components';
		self::setLang();
		$this->setDefaultLang();
		$this->setTexts( $componentName );
	}

	/**
	 * Returns text translated into present language.
	 *
	 * @param string $textKey Key representing translated text.
	 *
	 * @return string Translated text or empty string if key doesn't exists.
	 */
	public function e( $textKey ) {
		if ( isset( $this->texts[ $textKey ] ) ) {
			if ( isset( $this->texts[ $textKey ][ self::$actualLang ] ) ) {
				return $this->texts[ $textKey ][ self::$actualLang ];
			} else if ( isset( $this->texts[ $textKey ][ $this->defaultLang ] ) ) {
				return $this->texts[ $textKey ][ $this->defaultLang ];
			}
		}

		return '@TODO: ' . $textKey . '[' . self::$actualLang . ']';
	}

	/**
	 * Echoes text translated into present language.
	 *
	 * @deprecated Since v1.4.2
	 *
	 * @param string $textKey Key representing translated text.
	 */
	public function _e( $textKey ) {
		echo $this->e( $textKey );
	}

	/**
	 * Sets language.
	 * <ul>
	 * <li>By parameter it sets language for translations and for whole wordpress. This happens only if some language
	 * hasn't been set or loaded before. So, use this before any instance of this class.</li>
	 * <li>By get parameter "lang" (eq. ?lang=en).
	 * <li>Automatically by WP function "get_locale()" returning default language.</li>
	 * </ul>
	 *
	 * @param string $lang (Optional) Language iso (cs, en etc.).
	 */
	public static function setLang( $lang = '' ) {
		if ( empty( self::$actualLang ) ) {
			if ( ! empty( $lang ) ) {
				global $locale;
				if ( isset( $locale ) ) {
					$locale = $lang;
				}
				self::$actualLang = $lang;
			} else if ( isset( $_GET['lang'] ) ) {
				self::$actualLang = filter_input( INPUT_GET, 'lang', FILTER_SANITIZE_FULL_SPECIAL_CHARS );
			} else {
				self::$actualLang = substr( get_locale(), 0, 2 );
			}
		}
	}

	/**
	 * Sets default language translation if texts for current language are missing.
	 *
	 * @param string $defaultLang Two character language ISO code.
	 */
	public function setDefaultLang( $defaultLang = 'en' ) {
		if ( ! empty( $defaultLang ) ) {
			$this->defaultLang = $defaultLang;
		}
	}

	/**
	 * Sets texts from translational file json (LANG_FILE_NAME). At the begging it loads translations from global file,
	 * then it loads "local" translations. Local translation file must be named "lang.json". Must be in the same
	 * directory as file in which this method is called. Or $componentName must be given.
	 *
	 * @param string $componentName If empty, it automatically takes folder as component name.
	 */
	private function setTexts( $componentName ) {
		//automatic component name search
		if ( empty( $componentName ) ) {
			$backtrace = debug_backtrace();
			$l         = count( $backtrace );
			for ( $i = 1; $i < $l; $i ++ ) {
				if ( isset( $backtrace[ $i ]['file'] ) && strpos( $backtrace[ $i ]['file'], 'components' ) !== false ) {
					$componentName = basename( dirname( $backtrace[ $i ]['file'] ) );

					break;
				}
			}
		}

		//global
		if ( file_exists( self::$defaultModulesPath . '/' . $componentName . '/' . self::LANG_FILE_NAME ) ) {
			$this->texts = json_decode( file_get_contents( self::$defaultModulesPath . '/' . $componentName . '/' . self::LANG_FILE_NAME ), true );
		}

		//local
		if ( file_exists( get_template_directory() . '/components/' . $componentName . '/' . self::LANG_FILE_NAME ) ) {
			$locTexts = json_decode( file_get_contents( get_template_directory() . '/components/' . $componentName . '/' . self::LANG_FILE_NAME ), true );

			foreach ( $locTexts as $key => $locText ) {
				foreach ( $locText as $lang => $text ) {
					$this->texts[ $key ][ $lang ] = $text;
				}
			}
		}
	}
}
