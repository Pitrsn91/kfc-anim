<?php

namespace CD;

use \DateTime;

/**
 * Class FMS
 */
class Fms {
	/**
	 * @var string
	 */
	private $startDate;

	/**
	 * @var string
	 */
	private $endDate;

	/**
	 * @var integer
	 */
	private $fmsTimeout = 14;

	/**
	 * @var string
	 */
	private $backUrl;

	/**
	 * @var string
	 */
	private $buttonsAttr = '.cse-back';

	/**
	 * @var string
	 */
	private $quizUrl;

	/**
	 * @var string
	 */
	private $companyName = '';


	public function setStartDate($startDate = '') {
		$this->startDate = $startDate;
	}

	public function setEndDate($endDate = '') {
		$this->endDate = $endDate;
	}

	public function setQuizUrl($quizUrl = '') {
		$this->quizUrl = $quizUrl;
	}

	public function setCompanyName($companyName = '') {
		$this->companyName = $companyName;
	}

	/**
	 * Sets css selector of back button. This button opens fms modal.
	 * @param string $buttonsAttr selector of back button (e.g. .cse-back)
	 */
	public function setButtonsAttr($buttonsAttr = '') {
		$this->buttonsAttr = $buttonsAttr;
	}

	/**
	 * Sets timeout in days for storing user selected option. After timeout expires, fms modal can be showed again.
	 * @param string $fmsTimeout timeout duration in days (default timeout is 14 days)
	 */
	public function setFmsTimeout($fmsTimeout = '14') {
		$this->fmsTimeout = $fmsTimeout;
	}

	/**
	 * Sets default url for back button, used when http referer is not set.
	 * @param string $backUrl default url for back button
	 */
	public function setBackUrl($backUrl = '') {
		$this->backUrl = $backUrl;
	}

	private function getFmsTimeout() {
		return (1000 * 60 * 60 * 24 * $this->fmsTimeout);
	}

	private function getBackUrl() {
		return isset($_SERVER['HTTP_REFERER']) ? filter_var($_SERVER['HTTP_REFERER'], FILTER_SANITIZE_FULL_SPECIAL_CHARS) : $this->backUrl;
	}

	private function isValid() {

		$errors = array();

		if (empty($this->startDate)) {
			$errors[] = '@TODO: startDate is not SET';
		}

		if (empty($this->endDate)) {
			$errors[] = '@TODO: endDate is not SET or end ';
		}

		if (empty($this->quizUrl) || $this->quizUrl === 'http://questionpro.com/t/xxxx') {
			$errors[] = '@TODO: quizUrl is not SET';
		}

		if (empty($this->backUrl)) {
			$errors[] = '@TODO: backUrl is not SET';
		}

		foreach ($errors as $error) {
			echo '<pre style="background: yellow; color:black;padding: 8px 15px;margin-bottom: 5px;">'.$error.'</pre>';
		}

		return count($errors) === 0;
	}


	/**
	 * Prints fms modal when all mandatory parameters are set and datetime conditions are met.
	 */

	public function init() {
		$positionId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_FULL_SPECIAL_CHARS) ?: 0;
		$texts = new Lang("fms");
		$now = new DateTime();

		if($this->isValid() && $now > $this->startDate && $now < $this->endDate && $positionId !== 0) { ?>

			<div id="lmc-fms-overlay" class="hidden">
				<div id="lmc-fms" class="lmc-fms-window">
					<p><?= $texts->e('disclaimerTextTop'); ?></p>
					<div class="lmc-fms__button-wrapper">
						<a id="lmc-fms-block" class="lmc-fms__button lmc-fms__button--yes" href="<?= $this->quizUrl . '?custom1=' . $positionId; ?>"><?= $texts->e('buttonYes'); ?></a>
						<a class="lmc-fms__button lmc-fms__button--no" href="<?= $this->getBackUrl(); ?>"><?= $texts->e('buttonNo'); ?></a>
					</div>
					<p><?= $texts->e('disclaimerTextBottom'); ?></p>
					<p class="lmc-fms__signature">
						<?= $texts->e('disclaimerTextThanks'); ?><?= !empty($this->companyName) ? ', ' . $this->companyName : '' ?>
					</p>
				</div>
			</div>

			<script src="//presentation.lmc.cz/libs/fms/fms.3.0.0.min.js<?= GlobalVersion::get();?>"></script>

			<script>
				FMS.init({
					companyId: 'fms-<?= md5($this->companyName) ?>',
					buttonSelector: '<?= $this->buttonsAttr ?>',
					timeout: <?= $this->getFmsTimeout() ?>
				});
			</script>

			<?php
		}
	}
}
