<?php
/**
 * Model autoloader
 */
spl_autoload_register( function ( $className ) {
	static $classMap = [
		CD\Env::class             => 'Env.php',
		CD\Fms::class             => 'Fms.php',
		CD\GlobalVersion::class   => 'GlobalVersion.php',
		CD\Lang::class            => 'Lang.php',
		CD\ThemeConfigBase::class => 'ThemeConfigBase.php',
		CD\Version::class         => 'Version.php'
	];

	if ( isset( $classMap[ $className ] ) ) {
		require __DIR__ . '/CD/' . $classMap[ $className ];
	}
} );
