<?php
/*
 * Page list. Feel free to add or remove them.
 *
 * TODO: neprenaset na WP
 */

// Config
$webLanguage = 'cs_CZ';
$defaultPage = 'uvod';
$pageList    = array(
	'uvod'               => array( 'pageTitle' => 'Úvod' )
);

// Page loading
$urlPage   = isset( $_GET['page'] ) ? filter_input( INPUT_GET, 'page', FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : '';
$thisPage  = array_key_exists( $urlPage, $pageList ) ? $urlPage : $defaultPage;
$pageTitle = $pageList[ $thisPage ]['pageTitle'];
