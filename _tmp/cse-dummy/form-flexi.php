<?php if (isset($_GET['reply-sent'])) { ?>

	<div class="cse-cont cse-form-wrap cse-reply-form"><h1>13.10.11 Název pozice 5 _131011_103237</h1>
		<div class="cse-thnx"><p>Děkujeme Vám za vyplnění dotazníku<br></p></div>
	</div>

<?php } else if (isset($_GET['invalid'])) { ?>

	<div class="cse-cont cse-detail-wrap">
		<h3>Tato pozice již neplatí!</h3>
		<p>Je nám líto, ale požadovaná pozice již není platná - byla zaměstnavatelem stažena. Zkuste prosím příště odpovědět na pozici dříve.</p>
	</div>

<?php } else {
	$formType = (isset( $_GET['flexi']) && !empty($_GET['flexi'])) ? urlencode($_GET['flexi']) : "default";

	$flexiTypeClassName = "block cse-hint-type-sign";

	if ($formType == "2") {
		$flexiTypeClassName = "block cse-hint-type-text";
	} elseif ($formType == "3") {
		$flexiTypeClassName = "inline cse-hint-type-sign";
	} elseif ($formType == "4") {
		$flexiTypeClassName = "inline cse-hint-type-text";
	}
	?>

	<div id="cse-flexi" class="cse-cont cse-form-wrap cse-reply-form <?= $flexiTypeClassName;?>"><h1>13.10.11 Název pozice
			5 _131011_103237</h1>
		<p class="cse-header">Záhlaví interdum tempor eros, non semper purus commodo sed? Curabitur sit amet condimentum
			velit posuere. In interdum tempor eros, non semper purus commodo sed? Curabitur sit amet condimentum velit
			posuere.<br/></p>
		<div class="cse-block cse-wrap-gdpr"><p class="cse-gdpr-notice-intro">Pro umožnění výběrového řízení na danou
				pracovní pozici bude společnost Test Company s.r.o., jakožto správce, zpracovávat Vámi poskytnuté (či z
				veřejných zdrojů získané) osobní údaje, a to v souladu s obecným nařízením o ochraně osobních údajů (EU)
				2016/679. Jste srozuměni s tím, že zpracování osobních údajů bude pro správce provádět společnost LMC
				s.r.o., IČO: 264 41 381 prostřednictvím svých elektronických systémů. <span class="cse-label-gdpr">Zobrazit více</span>
			</p>
			<p class="cse-gdpr-notice-additional" style="display: none;"> Odesláním reakce na tento inzerát poskytujete
				správci své osobní údaje za účelem a po dobu realizace výběrového řízení. V souvislosti se zpracováním
				Vašich osobních údajů máte příslušná práva: (i) na přístup k osobním údajům, (ii) na opravu či doplnění
				nepřesných nebo nepravdivých osobních údajů, (iii) na výmaz osobních údajů, nejsou-li již osobní údaje
				potřebné pro účely, pro které byly shromážděny či jinak zpracovány, anebo zjistíte-li, že byly
				zpracovávány protiprávně, (iv) na omezení zpracování osobních údajů ve zvláštních případech (v) na
				přenositelnost údajů a (vi) vznést námitku, po níž zpracování Vašich osobních údajů bude ukončeno,
				neprokáže-li se, že existují závažné oprávněné důvody pro zpracování, jež převažují nad Vašimi zájmy
				nebo právy a svobodami zejména, je-li důvodem případné vymáhání právních nároků a (vii) obrátit se na
				Úřad pro ochranu osobních údajů. Další informace o zpracování údajů společností LMC s.r.o., IČO: 264 41
				381, se sídlem Praha 7, Jankovcova 1569/2c, PSČ 170 00 poskytuje pověřenec pro ochranu osobních údajů
				Jan Svoboda, e-mail: Jan.Svoboda@lmc.eu, resp. jsou dostupné na
				<a href="https://www.lmc.eu/cs/zasady-ochrany-soukromi/" target="_blank">https://www.lmc.eu/cs/zasady-ochrany-soukromi/</a>.
			</p></div>
		<form method="post" enctype="multipart/form-data" action="?id=240822803" data-lang="cs">
			<fieldset>
				<div class="cse-block cse-field-date cse-wrap-hrDate1">
					<label><span class="cse-label">Datum kalendář</span><input type="date" id="hrDate1" name="hrDate1" class="date" value="" title=""/></label>
				</div>
				<div class="cse-block cse-field-text cse-wrap-name">
					<label><span class="cse-label">Jméno<span class="cse-required"> * </span></span><input type="text" id="name" name="name" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^[a-žA-ŽА-Яа-я0-9][a-žA-ŽА-Яа-я0-9'-., ]*[a-žA-ŽА-Яа-я0-9'-.,]$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮJménoˮ zadejte ve správném formátu.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;100\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮJménoˮ můžete zadat maximálně 100 znaků.\&quot;},{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮJménoˮ je nutné zodpovědět.\&quot;}]}" required maxlength="100" pattern="(^$)|(^[a-žA-ŽА-Яа-я0-9][a-žA-ŽА-Яа-я0-9'-., ]*[a-žA-ŽА-Яа-я0-9'-.,]$)" title="Otázku &quot;Jméno&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Jméno&quot; můžete zadat maximálně 100 znaků. Odpověď na otázku &quot;Jméno&quot; zadejte ve správném formátu."/></label>
				</div>
				<div class="cse-block cse-field-text cse-wrap-surname">
					<label><span class="cse-label">Příjmení<span class="cse-required"> * </span></span><input type="text" id="surname" name="surname" class="text" value="příjmení" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^[a-žA-ŽА-Яа-я0-9][a-žA-ŽА-Яа-я0-9'-., ]*[a-žA-ŽА-Яа-я0-9'-.,]$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPříjmeníˮ zadejte ve správném formátu.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;100\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮPříjmeníˮ můžete zadat maximálně 100 znaků.\&quot;},{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPříjmeníˮ je nutné zodpovědět.\&quot;}]}" required maxlength="100" pattern="(^$)|(^[a-žA-ŽА-Яа-я0-9][a-žA-ŽА-Яа-я0-9'-., ]*[a-žA-ŽА-Яа-я0-9'-.,]$)" title="Otázku &quot;Příjmení&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Příjmení&quot; můžete zadat maximálně 100 znaků. Odpověď na otázku &quot;Příjmení&quot; zadejte ve správném formátu."/></label>
				</div>
				<div class="cse-block cse-field-text cse-wrap-email">
					<label><span class="cse-label">E-mail<span class="cse-required"> * </span></span><input type="text" id="email" name="email" class="text" value="email@email.cz" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^[-!#$%&amp;'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&amp;'*+\/0-9=?A-Z^_a-z`{|}~])*@(([a-zA-Z0-9]\.)|([a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])\.))+[a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮE-mailˮ zadejte ve správném formátu.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;60\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮE-mailˮ můžete zadat maximálně 60 znaků.\&quot;},{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮE-mailˮ je nutné zodpovědět.\&quot;}]}" required maxlength="60" pattern="(^$)|(^[-!#$%&amp;'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&amp;'*+\/0-9=?A-Z^_a-z`{|}~])*@(([a-zA-Z0-9]\.)|([a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])\.))+[a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])$)" title="Otázku &quot;E-mail&quot; je nutné zodpovědět. V odpovědi na otázku &quot;E-mail&quot; můžete zadat maximálně 60 znaků. Odpověď na otázku &quot;E-mail&quot; zadejte ve správném formátu."/></label>
				</div>
				<div class="cse-block cse-field-text cse-wrap-ttlBfr">
					<label><span class="cse-label">Titul</span><input type="text" id="ttlBfr" name="ttlBfr" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;50\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTitulˮ můžete zadat maximálně 50 znaků.\&quot;}]}" maxlength="50" title="V odpovědi na otázku &quot;Titul&quot; můžete zadat maximálně 50 znaků. "/><span class="cse-hint">Znění nápovědy:Titul</span></label>
				</div>
				<div class="cse-group" data-validation-required="0" data-validation-duplicate="telefon" data-repeatcount="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTelefonní čísloˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;telefon\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Telefonní číslo se nesmí opakovat.\&quot;}]}" data-required="required" data-duplicate="telefon" data-title="Otázku &quot;Telefonní číslo&quot; je nutné zodpovědět. Hodnota v poli Telefonní číslo se nesmí opakovat. ">
					<div data-number="0" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-telefon"><label><span class="cse-label">Telefonní číslo</span><input type="text" id="telefon_0" name="telefon[]" class="text" value="111111111" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^[\+]?[0-9]{9,15}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮTelefonní čísloˮ zadejte ve správném formátu.\&quot;}]}" pattern="(^$)|(^[\+]?[0-9]{9,15}$)" title="Odpověď na otázku &quot;Telefonní číslo&quot; zadejte ve správném formátu."/><span class="cse-hint">Znění nápovědy:Telefonní číslo</span></label>
						</div>
					</div>
					<div data-number="1" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-telefon"><label><span class="cse-label">Telefonní číslo</span><input type="text" id="telefon_1" name="telefon[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^[\+]?[0-9]{9,15}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮTelefonní čísloˮ zadejte ve správném formátu.\&quot;}]}" pattern="(^$)|(^[\+]?[0-9]{9,15}$)" title="Odpověď na otázku &quot;Telefonní číslo&quot; zadejte ve správném formátu."/><span class="cse-hint">Znění nápovědy:Telefonní číslo</span></label>
						</div>
					</div>
				</div>
				<div class="cse-group" data-validation-required="0" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydlištěˮ je nutné zodpovědět.\&quot;}]}" data-required="required" data-title="Otázku &quot;Trvalé bydliště&quot; je nutné zodpovědět. ">
					<h2>Trvalé bydliště</h2><span class="cse-hint">Znění nápovědy:Trvalé bydliště</span>
					<div class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-domicile_str"><label><span class="cse-label">Ulice<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="domicile_str" name="domicile_str" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydliště - Uliceˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;100\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - Uliceˮ můžete zadat maximálně 100 znaků.\&quot;}]}" data-required="required" maxlength="100" title="Otázku &quot;Trvalé bydliště - Ulice&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Trvalé bydliště - Ulice&quot; můžete zadat maximálně 100 znaků. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-domicile_cp">
							<label><span class="cse-label">č.p.<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="domicile_cp" name="domicile_cp" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydliště - č.p.ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;16\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - č.p.ˮ můžete zadat maximálně 16 znaků.\&quot;}]}" data-required="required" maxlength="16" title="Otázku &quot;Trvalé bydliště - č.p.&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Trvalé bydliště - č.p.&quot; můžete zadat maximálně 16 znaků. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-domicile_co">
							<label><span class="cse-label">č.o.</span><input type="text" id="domicile_co" name="domicile_co" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;16\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - č.o.ˮ můžete zadat maximálně 16 znaků.\&quot;}]}" maxlength="16" title="V odpovědi na otázku &quot;Trvalé bydliště - č.o.&quot; můžete zadat maximálně 16 znaků. "/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-text cse-wrap-domicile_zip">
							<label><span class="cse-label">PSČ<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="domicile_zip" name="domicile_zip" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydliště - PSČˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;16\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - PSČˮ můžete zadat maximálně 16 znaků.\&quot;}]}" data-required="required" maxlength="16" title="Otázku &quot;Trvalé bydliště - PSČ&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Trvalé bydliště - PSČ&quot; můžete zadat maximálně 16 znaků. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-domicile_city"><label><span class="cse-label">Město<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="domicile_city" name="domicile_city" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydliště - Městoˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;100\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - Městoˮ můžete zadat maximálně 100 znaků.\&quot;}]}" data-required="required" maxlength="100" title="Otázku &quot;Trvalé bydliště - Město&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Trvalé bydliště - Město&quot; můžete zadat maximálně 100 znaků. "/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-domicile_country">
							<label><span class="cse-label">Země<span class="cse-required cse-subrequired"> * </span></span><select id="domicile_country" name="domicile_country" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮTrvalé bydliště - Zeměˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;100\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮTrvalé bydliště - Zeměˮ můžete zadat maximálně 100 znaků.\&quot;}]}" data-required="required" title="Otázku &quot;Trvalé bydliště - Země&quot; je nutné zodpovědět. V odpovědi na otázku &quot;Trvalé bydliště - Země&quot; můžete zadat maximálně 100 znaků. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Afgánistán</option>
									<option value="3">Albánie</option>
									<option value="4">Alžírsko</option>
									<option value="5">Andorra</option>
									<option value="6">Angola</option>
									<option value="7">Antigua a Barbuda</option>
									<option value="8">Argentina</option>
									<option value="9">Arménie</option>
									<option value="10">Austrálie</option>
									<option value="11">Ázerbájdžán</option>
									<option value="12">Bahamy</option>
									<option value="13">Bahrajn</option>
									<option value="14">Bangladéš</option>
									<option value="15">Barbados</option>
									<option value="16">Belgie</option>
									<option value="17">Belize</option>
									<option value="18">Bělorusko</option>
									<option value="19">Benin</option>
									<option value="20">Bhútán</option>
									<option value="21">Bolívie</option>
									<option value="22">Bosna a Hercegovina</option>
									<option value="23">Botswana</option>
									<option value="24">Brazílie</option>
									<option value="25">Brunej</option>
									<option value="26">Bulharsko</option>
									<option value="27">Burkina Faso</option>
									<option value="28">Burundi</option>
									<option value="29">Čad</option>
									<option value="30">Česká republika</option>
									<option value="31">Čína</option>
									<option value="32">Dánsko</option>
									<option value="33">Dominika</option>
									<option value="34">Dominikánská republika</option>
									<option value="35">Džibutsko</option>
									<option value="36">Egypt</option>
									<option value="37">Ekvádor</option>
									<option value="38">Eritrea</option>
									<option value="39">Estonsko</option>
									<option value="40">Etiopie</option>
									<option value="41">Fidži</option>
									<option value="42">Filipíny</option>
									<option value="43">Finsko</option>
									<option value="44">Francie</option>
									<option value="45">Francouzská Guyana</option>
									<option value="46">Gabon</option>
									<option value="47">Gambie</option>
									<option value="48">Ghana</option>
									<option value="49">Gruzie</option>
									<option value="50">Guatemala</option>
									<option value="51">Guinea</option>
									<option value="52">Guinea-Bissau</option>
									<option value="53">Guyana</option>
									<option value="54">Haiti</option>
									<option value="55">Honduras</option>
									<option value="56">Chile</option>
									<option value="57">Chorvatsko</option>
									<option value="58">Indie</option>
									<option value="59">Indonésie</option>
									<option value="60">Irák</option>
									<option value="61">Írán</option>
									<option value="62">Irsko</option>
									<option value="63">Island</option>
									<option value="64">Itálie</option>
									<option value="65">Izrael</option>
									<option value="66">Jamajka</option>
									<option value="67">Japonsko</option>
									<option value="68">Jemen</option>
									<option value="69">Jihoafrická republika</option>
									<option value="70">Jižní Korea</option>
									<option value="71">Jordán</option>
									<option value="72">Kambodža</option>
									<option value="73">Kamerun</option>
									<option value="74">Kanada</option>
									<option value="75">Kapverdy</option>
									<option value="76">Katar</option>
									<option value="77">Kazachstán</option>
									<option value="78">Keňa</option>
									<option value="79">Kiribati</option>
									<option value="80">Kolumbie</option>
									<option value="81">Komory</option>
									<option value="82">Konžská demokratická republika</option>
									<option value="83">Konžská republika</option>
									<option value="84">Kostarika</option>
									<option value="85">Kuba</option>
									<option value="86">Kuvajt</option>
									<option value="87">Kypr</option>
									<option value="88">Kyrgyzstán</option>
									<option value="89">Laos</option>
									<option value="90">Lesotho</option>
									<option value="91">Libanon</option>
									<option value="92">Libérie</option>
									<option value="93">Libye</option>
									<option value="94">Lichtenštejnsko</option>
									<option value="95">Litva</option>
									<option value="96">Lotyšsko</option>
									<option value="97">Lucembursko</option>
									<option value="98">Madagaskar</option>
									<option value="99">Maďarsko</option>
									<option value="100">Makedonská republika</option>
									<option value="101">Malajsie</option>
									<option value="102">Malawi</option>
									<option value="103">Maledivy</option>
									<option value="104">Mali</option>
									<option value="105">Malta</option>
									<option value="106">Maroko</option>
									<option value="107">Marshallovy ostrovy</option>
									<option value="108">Mauricius</option>
									<option value="109">Mauritánie</option>
									<option value="110">Mexiko</option>
									<option value="111">Mikronéské federativní státy</option>
									<option value="112">Moldavsko</option>
									<option value="113">Monako</option>
									<option value="114">Mongolsko</option>
									<option value="115">Mosambik</option>
									<option value="116">Myanmar</option>
									<option value="117">Namibie</option>
									<option value="118">Nauru</option>
									<option value="119">Německo</option>
									<option value="120">Nepál</option>
									<option value="121">Niger</option>
									<option value="122">Nigérie</option>
									<option value="123">Nikaragua</option>
									<option value="124">Nizozemí</option>
									<option value="125">Norsko</option>
									<option value="126">Nový Zéland</option>
									<option value="127">Omán</option>
									<option value="128">Pákistán</option>
									<option value="129">Palau</option>
									<option value="130">Panama</option>
									<option value="131">Papua-Nová Guiena</option>
									<option value="132">Paraguay</option>
									<option value="133">Peru</option>
									<option value="134">Pobřeží slonoviny</option>
									<option value="135">Polsko</option>
									<option value="136">Portugalsko</option>
									<option value="137">Rakousko</option>
									<option value="138">Rovníková Guinea</option>
									<option value="139">Rumunsko</option>
									<option value="140">Rusko (Asijská část)</option>
									<option value="141">Rusko (Evropská část)</option>
									<option value="142">Rwanda</option>
									<option value="143">Řecko</option>
									<option value="144">Salvador</option>
									<option value="145">Samoa</option>
									<option value="146">San Marino</option>
									<option value="147">Saúdská Arábie</option>
									<option value="148">Senegal</option>
									<option value="149">Severní Korea</option>
									<option value="150">Seychely</option>
									<option value="151">Sierra Leone</option>
									<option value="152">Singapur</option>
									<option value="153">Slovensko</option>
									<option value="154">Slovinsko</option>
									<option value="155">Somálsko</option>
									<option value="156">Spojené arabské emiráty</option>
									<option value="157">Spojené státy americké</option>
									<option value="158">Srbsko a Černá Hora</option>
									<option value="159">Srí Lanka</option>
									<option value="160">Středoafrická republika</option>
									<option value="161">Súdán</option>
									<option value="162">Surinam</option>
									<option value="163">Svatá Lucie</option>
									<option value="164">Svatý Kryštof a Nevis</option>
									<option value="165">Svatý Tomáš a Princův ostrov</option>
									<option value="166">Svatý Vincent a Grenadiny</option>
									<option value="167">Svazijsko</option>
									<option value="168">Sýrie</option>
									<option value="169">Šalamounovy ostrovy</option>
									<option value="170">Španělsko</option>
									<option value="171">Švédsko</option>
									<option value="172">Švýcarsko</option>
									<option value="173">Tádžikistán</option>
									<option value="174">Tanzanie</option>
									<option value="175">Thajsko</option>
									<option value="176">Tchaj-wan</option>
									<option value="177">Togo</option>
									<option value="178">Tonga</option>
									<option value="179">Trinidad a Tobago</option>
									<option value="180">Tunisko</option>
									<option value="181">Turecko</option>
									<option value="182">Turkmenistán</option>
									<option value="183">Tuvalu</option>
									<option value="184">Uganda</option>
									<option value="185">Ukrajina</option>
									<option value="186">Uruguay</option>
									<option value="187">Uzbekistán</option>
									<option value="188">Vanuatu</option>
									<option value="189">Vatikán</option>
									<option value="190">Velká Británie a Severní Irsko</option>
									<option value="191">Venezuela</option>
									<option value="192">Vietnam</option>
									<option value="193">Zambie</option>
									<option value="194">Zimbabwe</option>
								</select></label></div>
					</div>
				</div>
				<h2 class="cse-separator-hidden">Separator - Vodorovná linka</h2>
				<hr class="cse-hr"/>
				<div class="cse-block cse-field-select cse-wrap-hgstEducation"><label><span class="cse-label">Nejvyšší dosažené vzdělání</span><select id="hgstEducation" name="hgstEducation" class="select" data-defaultValue="1" title="">
							<option value="1" selected="selected">- vyberte -</option>
							<option value="2">Základní</option>
							<option value="3">Odborné vyučení bez maturity</option>
							<option value="4">Středoškolské nebo odborné vyučení s maturitou</option>
							<option value="5">Vyšší odborné</option>
							<option value="6">Bakalářské</option>
							<option value="7">Vysokoškolské/ universitní</option>
							<option value="8">MBA, MBT, postgraduální studium</option>
						</select><span class="cse-hint">Znění nápovědy:Nejvyšší dosažené vzdělání</span></label></div>
				<div class="cse-group" data-validation-required="0" data-validation-duplicate="education_schl_nm,education_fld,education_strt_yr,education_end_yr" data-validation-function="validateDateRange:education_strt_yr,null,education_end_yr,null" data-repeatcount="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkolyˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;education_schl_nm,education_fld,education_strt_yr,education_end_yr\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Školy se nesmí opakovat.\&quot;},{\&quot;type\&quot;:\&quot;function\&quot;,\&quot;condition\&quot;:\&quot;validateDateRange:education_strt_yr,null,education_end_yr,null\&quot;,\&quot;message\&quot;:\&quot;Datum ˮodˮ v otázce ˮŠkolyˮ musi být menší než datum ˮdoˮ a zároveň nesmí být datumy v budoucnu.\&quot;}]}" data-required="required" data-duplicate="education_schl_nm,education_fld,education_strt_yr,education_end_yr" data-function="validateDateRange:education_strt_yr,null,education_end_yr,null" data-title="Otázku &quot;Školy&quot; je nutné zodpovědět. Hodnota v poli Školy se nesmí opakovat. Datum &quot;od&quot; v otázce &quot;Školy&quot; musi být menší než datum &quot;do&quot; a zároveň nesmí být datumy v budoucnu. ">
					<h2>Školy</h2><span class="cse-hint">Znění nápovědy:Školy</span>
					<div data-number="0" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-education_schl_nm"><label><span class="cse-label">Název školy<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_schl_nm_0" name="education_schl_nm[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Název školyˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Školy - Název školy&quot; je nutné zodpovědět. "/></label>
						</div>
						<div class="cse-block cse-field-select cse-wrap-education_fld"><label><span class="cse-label">Obor<span class="cse-required cse-subrequired"> * </span></span><select id="education_fld_0" name="education_fld[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Oborˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Školy - Obor&quot; je nutné zodpovědět. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Architektura a urbanismus</option>
									<option value="3">Bez určení</option>
									<option value="4">Biologické obory</option>
									<option value="5">Doprava a spoje</option>
									<option value="6">Ekologie a ochrana životního prostředí</option>
									<option value="7">Ekonomie</option>
									<option value="8">Ekonomika a administrativa</option>
									<option value="9">Elektrotechnika, telekomunikační a výpočetní technika</option>
									<option value="10">Farmacie</option>
									<option value="11">Filologie</option>
									<option value="12">Filozofie, teologie</option>
									<option value="13">Fyzikální obory</option>
									<option value="14">Gastronomie, hotelnictví a turismus</option>
									<option value="15">Geografické obory</option>
									<option value="16">Geologické obory</option>
									<option value="17">Hornictví a hornická geologie, hutnictví a slévárenství</option>
									<option value="18">Chemické obory</option>
									<option value="19">Informatické obory</option>
									<option value="20">Kožedělná a obuvnická výroba a zpracování plastů</option>
									<option value="21">Lékařství</option>
									<option value="22">Manažersko-technické zabezpečení vzdušných sil</option>
									<option value="23">Matematické obory</option>
									<option value="24">Obecná příprava</option>
									<option value="25">Obecně odborná příprava</option>
									<option value="26">Obchod</option>
									<option value="27">Obory z oblasti historie</option>
									<option value="28">Obory z oblasti psychologie</option>
									<option value="29">Osobní a provozní služby</option>
									<option value="30">Pedagogika, učitelství a sociální péče</option>
									<option value="31">Podnikání v oborech, odvětví</option>
									<option value="32">Politologie</option>
									<option value="33">Polygrafie, zpracování papíru, filmu a fotografie</option>
									<option value="34">Potravinářství a potravinářská chemie</option>
									<option value="35">Právo, právní a veřejnosprávní činnost</option>
									<option value="36">Publicistika, knihovnictví a informatika</option>
									<option value="37">Reklama, PR, marketing</option>
									<option value="38">Speciální a interdisciplinární technické obory</option>
									<option value="39">Stavebnictví, geodézie a kartografie</option>
									<option value="40">Strojírenství a strojírenská výroba</option>
									<option value="41">Technická chemie a chemie silikátů</option>
									<option value="42">Tělesná kultura, tělovýchova a sport</option>
									<option value="43">Teorie ekonomie obrany státu</option>
									<option value="44">Teorie umění</option>
									<option value="45">Teorie vojenského umění</option>
									<option value="46">Textilní výroba a oděvnictví</option>
									<option value="47">Umění a užité umění</option>
									<option value="48">Veterinářství a veterinární prevence</option>
									<option value="49">Vojenské zdravotnictví</option>
									<option value="50">Vojenský letový provoz</option>
									<option value="51">Zdravotnictví</option>
									<option value="52">Zemědělství a lesnictví</option>
									<option value="53">Zpracování dřeva a výroba hudebních nástrojů</option>
								</select></label></div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-text cse-wrap-education_strt_yr"><label><span class="cse-label">Rok zahájení<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_strt_yr_0" name="education_strt_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Rok zahájeníˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮŠkoly - Rok zahájeníˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Školy - Rok zahájení&quot; je nutné zodpovědět. Odpověď na otázku &quot;Školy - Rok zahájení&quot; zadejte ve správném formátu."/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-education_end_yr"><label><span class="cse-label">Rok ukončení<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_end_yr_0" name="education_end_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Rok ukončeníˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮŠkoly - Rok ukončeníˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Školy - Rok ukončení&quot; je nutné zodpovědět. Odpověď na otázku &quot;Školy - Rok ukončení&quot; zadejte ve správném formátu."/></label>
						</div>
					</div>
					<div data-number="1" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-education_schl_nm"><label><span class="cse-label">Název školy<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_schl_nm_1" name="education_schl_nm[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Název školyˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Školy - Název školy&quot; je nutné zodpovědět. "/></label>
						</div>
						<div class="cse-block cse-field-select cse-wrap-education_fld"><label><span class="cse-label">Obor<span class="cse-required cse-subrequired"> * </span></span><select id="education_fld_1" name="education_fld[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Oborˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Školy - Obor&quot; je nutné zodpovědět. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Architektura a urbanismus</option>
									<option value="3">Bez určení</option>
									<option value="4">Biologické obory</option>
									<option value="5">Doprava a spoje</option>
									<option value="6">Ekologie a ochrana životního prostředí</option>
									<option value="7">Ekonomie</option>
									<option value="8">Ekonomika a administrativa</option>
									<option value="9">Elektrotechnika, telekomunikační a výpočetní technika</option>
									<option value="10">Farmacie</option>
									<option value="11">Filologie</option>
									<option value="12">Filozofie, teologie</option>
									<option value="13">Fyzikální obory</option>
									<option value="14">Gastronomie, hotelnictví a turismus</option>
									<option value="15">Geografické obory</option>
									<option value="16">Geologické obory</option>
									<option value="17">Hornictví a hornická geologie, hutnictví a slévárenství</option>
									<option value="18">Chemické obory</option>
									<option value="19">Informatické obory</option>
									<option value="20">Kožedělná a obuvnická výroba a zpracování plastů</option>
									<option value="21">Lékařství</option>
									<option value="22">Manažersko-technické zabezpečení vzdušných sil</option>
									<option value="23">Matematické obory</option>
									<option value="24">Obecná příprava</option>
									<option value="25">Obecně odborná příprava</option>
									<option value="26">Obchod</option>
									<option value="27">Obory z oblasti historie</option>
									<option value="28">Obory z oblasti psychologie</option>
									<option value="29">Osobní a provozní služby</option>
									<option value="30">Pedagogika, učitelství a sociální péče</option>
									<option value="31">Podnikání v oborech, odvětví</option>
									<option value="32">Politologie</option>
									<option value="33">Polygrafie, zpracování papíru, filmu a fotografie</option>
									<option value="34">Potravinářství a potravinářská chemie</option>
									<option value="35">Právo, právní a veřejnosprávní činnost</option>
									<option value="36">Publicistika, knihovnictví a informatika</option>
									<option value="37">Reklama, PR, marketing</option>
									<option value="38">Speciální a interdisciplinární technické obory</option>
									<option value="39">Stavebnictví, geodézie a kartografie</option>
									<option value="40">Strojírenství a strojírenská výroba</option>
									<option value="41">Technická chemie a chemie silikátů</option>
									<option value="42">Tělesná kultura, tělovýchova a sport</option>
									<option value="43">Teorie ekonomie obrany státu</option>
									<option value="44">Teorie umění</option>
									<option value="45">Teorie vojenského umění</option>
									<option value="46">Textilní výroba a oděvnictví</option>
									<option value="47">Umění a užité umění</option>
									<option value="48">Veterinářství a veterinární prevence</option>
									<option value="49">Vojenské zdravotnictví</option>
									<option value="50">Vojenský letový provoz</option>
									<option value="51">Zdravotnictví</option>
									<option value="52">Zemědělství a lesnictví</option>
									<option value="53">Zpracování dřeva a výroba hudebních nástrojů</option>
								</select></label></div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-text cse-wrap-education_strt_yr"><label><span class="cse-label">Rok zahájení<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_strt_yr_1" name="education_strt_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Rok zahájeníˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮŠkoly - Rok zahájeníˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Školy - Rok zahájení&quot; je nutné zodpovědět. Odpověď na otázku &quot;Školy - Rok zahájení&quot; zadejte ve správném formátu."/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-education_end_yr"><label><span class="cse-label">Rok ukončení<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="education_end_yr_1" name="education_end_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮŠkoly - Rok ukončeníˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮŠkoly - Rok ukončeníˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Školy - Rok ukončení&quot; je nutné zodpovědět. Odpověď na otázku &quot;Školy - Rok ukončení&quot; zadejte ve správném formátu."/></label>
						</div>
					</div>
				</div>
				<div class="cse-block cse-field-select cse-wrap-absolvent">
					<label><span class="cse-label">Absolvent</span><select id="absolvent" name="absolvent" class="select" data-defaultValue="1" title="">
							<option value="1" selected="selected">- vyberte -</option>
							<option value="2">Ano</option>
							<option value="3">Ne</option>
						</select><span class="cse-hint">Znění nápovědy:Absolvent</span></label></div>
				<div class="cse-group" data-validation-required="0" data-validation-duplicate="praxis_cmp,praxis_pos,praxis_fld,praxis_strt_mnth,praxis_strt_yr,praxis_end_mnth,praxis_end_yr" data-validation-function="validateDateRange:praxis_strt_yr,praxis_strt_mnth,praxis_end_yr,praxis_end_mnth" data-repeatcount="2" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxeˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;praxis_cmp,praxis_pos,praxis_fld,praxis_strt_mnth,praxis_strt_yr,praxis_end_mnth,praxis_end_yr\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Praxe se nesmí opakovat.\&quot;},{\&quot;type\&quot;:\&quot;function\&quot;,\&quot;condition\&quot;:\&quot;validateDateRange:praxis_strt_yr,praxis_strt_mnth,praxis_end_yr,praxis_end_mnth\&quot;,\&quot;message\&quot;:\&quot;Datum ˮodˮ v otázce ˮPraxeˮ musi být menší než datum ˮdoˮ a zároveň nesmí být datumy v budoucnu. Datum ˮdoˮ musí být buď nezadáno, zadáno jako ˮdosudˮ bez konkrétního roku nebo vyplněno kompletně.\&quot;}]}" data-required="required" data-duplicate="praxis_cmp,praxis_pos,praxis_fld,praxis_strt_mnth,praxis_strt_yr,praxis_end_mnth,praxis_end_yr" data-function="validateDateRange:praxis_strt_yr,praxis_strt_mnth,praxis_end_yr,praxis_end_mnth" data-title="Otázku &quot;Praxe&quot; je nutné zodpovědět. Hodnota v poli Praxe se nesmí opakovat. Datum &quot;od&quot; v otázce &quot;Praxe&quot; musi být menší než datum &quot;do&quot; a zároveň nesmí být datumy v budoucnu. Datum &quot;do&quot; musí být buď nezadáno, zadáno jako &quot;dosud&quot; bez konkrétního roku nebo vyplněno kompletně. ">
					<h2>Praxe</h2><span class="cse-hint">Znění nápovědy:Praxe</span>
					<div data-number="0" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-praxis_cmp"><label><span class="cse-label">Zaměstnavatel<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_cmp_0" name="praxis_cmp[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Zaměstnavatelˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Zaměstnavatel&quot; je nutné zodpovědět. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-praxis_pos"><label><span class="cse-label">Pracovní pozice<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_pos_0" name="praxis_pos[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Pracovní poziceˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Pracovní pozice&quot; je nutné zodpovědět. "/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_fld">
							<label><span class="cse-label">Obor<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_fld_0" name="praxis_fld[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Oborˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Obor&quot; je nutné zodpovědět. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Administrativa</option>
									<option value="3">Auto - moto</option>
									<option value="4">Bankovnictví a finanční služby</option>
									<option value="5">Cestovní ruch a ubytování</option>
									<option value="6">Doprava, logistika a zásobování</option>
									<option value="7">Ekonomika a podnikové finance</option>
									<option value="8">Elektrotechnika a energetika</option>
									<option value="9">Farmacie</option>
									<option value="10">Gastronomie a pohostinství</option>
									<option value="11">Chemický průmysl</option>
									<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
									<option value="13">IS/IT: Správa systémů a HW</option>
									<option value="14">IS/IT: Vývoj aplikací a systémů</option>
									<option value="15">Kultura, umění a tvůrčí práce</option>
									<option value="16">Kvalita a kontrola jakosti</option>
									<option value="17">Marketing</option>
									<option value="18">Média, reklama a PR</option>
									<option value="19">Nákup</option>
									<option value="20">Ostraha a bezpečnost</option>
									<option value="21">Personalistika a HR</option>
									<option value="22">Pojišťovnictví</option>
									<option value="23">Potravinářství</option>
									<option value="24">Právní služby</option>
									<option value="25">Prodej a obchod</option>
									<option value="26">Řemeslné a manuální práce</option>
									<option value="27">Služby</option>
									<option value="28">Státní a veřejná správa</option>
									<option value="29">Stavebnictví a reality</option>
									<option value="30">Strojírenství</option>
									<option value="31">Technika a vývoj</option>
									<option value="32">Telekomunikace</option>
									<option value="33">Věda a výzkum</option>
									<option value="34">Vrcholový management</option>
									<option value="35">Vydavatelství, tisk a polygrafie</option>
									<option value="36">Výroba a průmysl</option>
									<option value="37">Vzdělávání a školství</option>
									<option value="38">Zákaznický servis</option>
									<option value="39">Zdravotnictví a sociální péče</option>
									<option value="40">Zemědělství, lesnictví a ekologie</option>
								</select></label></div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_strt_mnth">
							<label><span class="cse-label">Od (měsíc)<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_strt_mnth_0" name="praxis_strt_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (měsíc)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Od (měsíc)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">leden</option>
									<option value="3">únor</option>
									<option value="4">březen</option>
									<option value="5">duben</option>
									<option value="6">květen</option>
									<option value="7">červen</option>
									<option value="8">červenec</option>
									<option value="9">srpen</option>
									<option value="10">září</option>
									<option value="11">říjen</option>
									<option value="12">listopad</option>
									<option value="13">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_strt_yr"><label><span class="cse-label">Od (rok)<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_strt_yr_0" name="praxis_strt_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (rok)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (rok)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Praxe - Od (rok)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_end_mnth"><label><span class="cse-label">Do (měsíc)</span><select id="praxis_end_mnth_0" name="praxis_end_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" title="Odpověď na otázku &quot;Praxe - Do (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">dosud</option>
									<option value="3">leden</option>
									<option value="4">únor</option>
									<option value="5">březen</option>
									<option value="6">duben</option>
									<option value="7">květen</option>
									<option value="8">červen</option>
									<option value="9">červenec</option>
									<option value="10">srpen</option>
									<option value="11">září</option>
									<option value="12">říjen</option>
									<option value="13">listopad</option>
									<option value="14">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_end_yr"><label><span class="cse-label">Do (rok)</span><input type="text" id="praxis_end_yr_0" name="praxis_end_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (rok)ˮ zadejte ve správném formátu.\&quot;}]}" pattern="(^$)|(^(19|20)\d{2}$)" title="Odpověď na otázku &quot;Praxe - Do (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
					</div>
					<div data-number="1" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-praxis_cmp"><label><span class="cse-label">Zaměstnavatel<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_cmp_1" name="praxis_cmp[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Zaměstnavatelˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Zaměstnavatel&quot; je nutné zodpovědět. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-praxis_pos"><label><span class="cse-label">Pracovní pozice<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_pos_1" name="praxis_pos[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Pracovní poziceˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Pracovní pozice&quot; je nutné zodpovědět. "/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_fld">
							<label><span class="cse-label">Obor<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_fld_1" name="praxis_fld[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Oborˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Obor&quot; je nutné zodpovědět. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Administrativa</option>
									<option value="3">Auto - moto</option>
									<option value="4">Bankovnictví a finanční služby</option>
									<option value="5">Cestovní ruch a ubytování</option>
									<option value="6">Doprava, logistika a zásobování</option>
									<option value="7">Ekonomika a podnikové finance</option>
									<option value="8">Elektrotechnika a energetika</option>
									<option value="9">Farmacie</option>
									<option value="10">Gastronomie a pohostinství</option>
									<option value="11">Chemický průmysl</option>
									<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
									<option value="13">IS/IT: Správa systémů a HW</option>
									<option value="14">IS/IT: Vývoj aplikací a systémů</option>
									<option value="15">Kultura, umění a tvůrčí práce</option>
									<option value="16">Kvalita a kontrola jakosti</option>
									<option value="17">Marketing</option>
									<option value="18">Média, reklama a PR</option>
									<option value="19">Nákup</option>
									<option value="20">Ostraha a bezpečnost</option>
									<option value="21">Personalistika a HR</option>
									<option value="22">Pojišťovnictví</option>
									<option value="23">Potravinářství</option>
									<option value="24">Právní služby</option>
									<option value="25">Prodej a obchod</option>
									<option value="26">Řemeslné a manuální práce</option>
									<option value="27">Služby</option>
									<option value="28">Státní a veřejná správa</option>
									<option value="29">Stavebnictví a reality</option>
									<option value="30">Strojírenství</option>
									<option value="31">Technika a vývoj</option>
									<option value="32">Telekomunikace</option>
									<option value="33">Věda a výzkum</option>
									<option value="34">Vrcholový management</option>
									<option value="35">Vydavatelství, tisk a polygrafie</option>
									<option value="36">Výroba a průmysl</option>
									<option value="37">Vzdělávání a školství</option>
									<option value="38">Zákaznický servis</option>
									<option value="39">Zdravotnictví a sociální péče</option>
									<option value="40">Zemědělství, lesnictví a ekologie</option>
								</select></label></div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_strt_mnth">
							<label><span class="cse-label">Od (měsíc)<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_strt_mnth_1" name="praxis_strt_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (měsíc)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Od (měsíc)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">leden</option>
									<option value="3">únor</option>
									<option value="4">březen</option>
									<option value="5">duben</option>
									<option value="6">květen</option>
									<option value="7">červen</option>
									<option value="8">červenec</option>
									<option value="9">srpen</option>
									<option value="10">září</option>
									<option value="11">říjen</option>
									<option value="12">listopad</option>
									<option value="13">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_strt_yr"><label><span class="cse-label">Od (rok)<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_strt_yr_1" name="praxis_strt_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (rok)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (rok)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Praxe - Od (rok)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_end_mnth"><label><span class="cse-label">Do (měsíc)</span><select id="praxis_end_mnth_1" name="praxis_end_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" title="Odpověď na otázku &quot;Praxe - Do (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">dosud</option>
									<option value="3">leden</option>
									<option value="4">únor</option>
									<option value="5">březen</option>
									<option value="6">duben</option>
									<option value="7">květen</option>
									<option value="8">červen</option>
									<option value="9">červenec</option>
									<option value="10">srpen</option>
									<option value="11">září</option>
									<option value="12">říjen</option>
									<option value="13">listopad</option>
									<option value="14">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_end_yr"><label><span class="cse-label">Do (rok)</span><input type="text" id="praxis_end_yr_1" name="praxis_end_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (rok)ˮ zadejte ve správném formátu.\&quot;}]}" pattern="(^$)|(^(19|20)\d{2}$)" title="Odpověď na otázku &quot;Praxe - Do (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
					</div>
					<div data-number="2" class="cse-panel">
						<div class="cse-block cse-field-text cse-wrap-praxis_cmp"><label><span class="cse-label">Zaměstnavatel<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_cmp_2" name="praxis_cmp[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Zaměstnavatelˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Zaměstnavatel&quot; je nutné zodpovědět. "/></label>
						</div>
						<div class="cse-block cse-field-text cse-wrap-praxis_pos"><label><span class="cse-label">Pracovní pozice<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_pos_2" name="praxis_pos[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Pracovní poziceˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Pracovní pozice&quot; je nutné zodpovědět. "/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_fld">
							<label><span class="cse-label">Obor<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_fld_2" name="praxis_fld[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Oborˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Obor&quot; je nutné zodpovědět. ">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">Administrativa</option>
									<option value="3">Auto - moto</option>
									<option value="4">Bankovnictví a finanční služby</option>
									<option value="5">Cestovní ruch a ubytování</option>
									<option value="6">Doprava, logistika a zásobování</option>
									<option value="7">Ekonomika a podnikové finance</option>
									<option value="8">Elektrotechnika a energetika</option>
									<option value="9">Farmacie</option>
									<option value="10">Gastronomie a pohostinství</option>
									<option value="11">Chemický průmysl</option>
									<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
									<option value="13">IS/IT: Správa systémů a HW</option>
									<option value="14">IS/IT: Vývoj aplikací a systémů</option>
									<option value="15">Kultura, umění a tvůrčí práce</option>
									<option value="16">Kvalita a kontrola jakosti</option>
									<option value="17">Marketing</option>
									<option value="18">Média, reklama a PR</option>
									<option value="19">Nákup</option>
									<option value="20">Ostraha a bezpečnost</option>
									<option value="21">Personalistika a HR</option>
									<option value="22">Pojišťovnictví</option>
									<option value="23">Potravinářství</option>
									<option value="24">Právní služby</option>
									<option value="25">Prodej a obchod</option>
									<option value="26">Řemeslné a manuální práce</option>
									<option value="27">Služby</option>
									<option value="28">Státní a veřejná správa</option>
									<option value="29">Stavebnictví a reality</option>
									<option value="30">Strojírenství</option>
									<option value="31">Technika a vývoj</option>
									<option value="32">Telekomunikace</option>
									<option value="33">Věda a výzkum</option>
									<option value="34">Vrcholový management</option>
									<option value="35">Vydavatelství, tisk a polygrafie</option>
									<option value="36">Výroba a průmysl</option>
									<option value="37">Vzdělávání a školství</option>
									<option value="38">Zákaznický servis</option>
									<option value="39">Zdravotnictví a sociální péče</option>
									<option value="40">Zemědělství, lesnictví a ekologie</option>
								</select></label></div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_strt_mnth">
							<label><span class="cse-label">Od (měsíc)<span class="cse-required cse-subrequired"> * </span></span><select id="praxis_strt_mnth_2" name="praxis_strt_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (měsíc)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" title="Otázku &quot;Praxe - Od (měsíc)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">leden</option>
									<option value="3">únor</option>
									<option value="4">březen</option>
									<option value="5">duben</option>
									<option value="6">květen</option>
									<option value="7">červen</option>
									<option value="8">červenec</option>
									<option value="9">srpen</option>
									<option value="10">září</option>
									<option value="11">říjen</option>
									<option value="12">listopad</option>
									<option value="13">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_strt_yr"><label><span class="cse-label">Od (rok)<span class="cse-required cse-subrequired"> * </span></span><input type="text" id="praxis_strt_yr_2" name="praxis_strt_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPraxe - Od (rok)ˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Od (rok)ˮ zadejte ve správném formátu.\&quot;}]}" data-required="required" pattern="(^$)|(^(19|20)\d{2}$)" title="Otázku &quot;Praxe - Od (rok)&quot; je nutné zodpovědět. Odpověď na otázku &quot;Praxe - Od (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
						<hr class="cse-line"/>
						<div class="cse-block cse-field-select cse-wrap-praxis_end_mnth"><label><span class="cse-label">Do (měsíc)</span><select id="praxis_end_mnth_2" name="praxis_end_mnth[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (měsíc)ˮ zadejte ve správném formátu.\&quot;}]}" title="Odpověď na otázku &quot;Praxe - Do (měsíc)&quot; zadejte ve správném formátu.">
									<option value="1" selected="selected">- vyberte -</option>
									<option value="2">dosud</option>
									<option value="3">leden</option>
									<option value="4">únor</option>
									<option value="5">březen</option>
									<option value="6">duben</option>
									<option value="7">květen</option>
									<option value="8">červen</option>
									<option value="9">červenec</option>
									<option value="10">srpen</option>
									<option value="11">září</option>
									<option value="12">říjen</option>
									<option value="13">listopad</option>
									<option value="14">prosinec</option>
								</select></label></div>
						<div class="cse-block cse-field-text cse-wrap-praxis_end_yr"><label><span class="cse-label">Do (rok)</span><input type="text" id="praxis_end_yr_2" name="praxis_end_yr[]" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^(19|20)\d{2}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPraxe - Do (rok)ˮ zadejte ve správném formátu.\&quot;}]}" pattern="(^$)|(^(19|20)\d{2}$)" title="Odpověď na otázku &quot;Praxe - Do (rok)&quot; zadejte ve správném formátu."/></label>
						</div>
					</div>
				</div>
				<div class="cse-border"><h2>Separator - Rámeček</h2>
					<div class="cse-group" data-validation-required="0" data-validation-duplicate="langSkill_lang" data-repeatcount="3" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazykaˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;langSkill_lang\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Znalost jazyka se nesmí opakovat.\&quot;}]}" data-required="required" data-duplicate="langSkill_lang" data-title="Otázku &quot;Znalost jazyka&quot; je nutné zodpovědět. Hodnota v poli Znalost jazyka se nesmí opakovat. ">
						<h3>Znalost jazyka</h3><span class="cse-hint">Znění nápovědy:Znalost jazyka</span>
						<div data-number="0" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-langSkill_lang">
								<label><span class="cse-label">Jazyk<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lang_0" name="langSkill_lang[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Jazykˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Jazyk&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Afrikánština</option>
										<option value="3">Albánština</option>
										<option value="4">Angličtina</option>
										<option value="5">Arabština</option>
										<option value="6">Baskičtina</option>
										<option value="7">Běloruština</option>
										<option value="8">Bulharština</option>
										<option value="9">Čeština</option>
										<option value="10">Čínština</option>
										<option value="11">Dánština</option>
										<option value="12">Estonština</option>
										<option value="13">Finština</option>
										<option value="14">Francouzština</option>
										<option value="15">Hebrejština</option>
										<option value="16">Holandština</option>
										<option value="17">Chorvatština</option>
										<option value="18">Indonézština</option>
										<option value="19">Islandština</option>
										<option value="20">Italština</option>
										<option value="21">Japonština</option>
										<option value="22">Katalánština</option>
										<option value="23">Korejština</option>
										<option value="24">Litevština</option>
										<option value="25">Lotyšština</option>
										<option value="26">Maďarština</option>
										<option value="27">Němčina</option>
										<option value="28">Norština</option>
										<option value="29">Polština</option>
										<option value="30">Portugalština</option>
										<option value="31">Rumunština</option>
										<option value="32">Ruština</option>
										<option value="33">Řečtina</option>
										<option value="34">Slovenština</option>
										<option value="35">Slovinština</option>
										<option value="36">Srbština</option>
										<option value="37">Španělština</option>
										<option value="38">Švédština</option>
										<option value="39">Thajština</option>
										<option value="40">Turečtina</option>
										<option value="41">Ukrajinština</option>
										<option value="42">Vietnamština</option>
									</select></label></div>
							<div class="cse-block cse-field-select cse-wrap-langSkill_lvl">
								<label><span class="cse-label">Úroveň<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lvl_0" name="langSkill_lvl[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Úroveňˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Úroveň&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Základní/Pasivní</option>
										<option value="3">Mírně pokročilá</option>
										<option value="4">Středně pokročilá</option>
										<option value="5">Pokročilá</option>
										<option value="6">Výborná/rodilý mluvčí</option>
									</select></label></div>
						</div>
						<div data-number="1" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-langSkill_lang">
								<label><span class="cse-label">Jazyk<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lang_1" name="langSkill_lang[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Jazykˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Jazyk&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Afrikánština</option>
										<option value="3">Albánština</option>
										<option value="4">Angličtina</option>
										<option value="5">Arabština</option>
										<option value="6">Baskičtina</option>
										<option value="7">Běloruština</option>
										<option value="8">Bulharština</option>
										<option value="9">Čeština</option>
										<option value="10">Čínština</option>
										<option value="11">Dánština</option>
										<option value="12">Estonština</option>
										<option value="13">Finština</option>
										<option value="14">Francouzština</option>
										<option value="15">Hebrejština</option>
										<option value="16">Holandština</option>
										<option value="17">Chorvatština</option>
										<option value="18">Indonézština</option>
										<option value="19">Islandština</option>
										<option value="20">Italština</option>
										<option value="21">Japonština</option>
										<option value="22">Katalánština</option>
										<option value="23">Korejština</option>
										<option value="24">Litevština</option>
										<option value="25">Lotyšština</option>
										<option value="26">Maďarština</option>
										<option value="27">Němčina</option>
										<option value="28">Norština</option>
										<option value="29">Polština</option>
										<option value="30">Portugalština</option>
										<option value="31">Rumunština</option>
										<option value="32">Ruština</option>
										<option value="33">Řečtina</option>
										<option value="34">Slovenština</option>
										<option value="35">Slovinština</option>
										<option value="36">Srbština</option>
										<option value="37">Španělština</option>
										<option value="38">Švédština</option>
										<option value="39">Thajština</option>
										<option value="40">Turečtina</option>
										<option value="41">Ukrajinština</option>
										<option value="42">Vietnamština</option>
									</select></label></div>
							<div class="cse-block cse-field-select cse-wrap-langSkill_lvl">
								<label><span class="cse-label">Úroveň<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lvl_1" name="langSkill_lvl[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Úroveňˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Úroveň&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Základní/Pasivní</option>
										<option value="3">Mírně pokročilá</option>
										<option value="4">Středně pokročilá</option>
										<option value="5">Pokročilá</option>
										<option value="6">Výborná/rodilý mluvčí</option>
									</select></label></div>
						</div>
						<div data-number="2" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-langSkill_lang">
								<label><span class="cse-label">Jazyk<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lang_2" name="langSkill_lang[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Jazykˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Jazyk&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Afrikánština</option>
										<option value="3">Albánština</option>
										<option value="4">Angličtina</option>
										<option value="5">Arabština</option>
										<option value="6">Baskičtina</option>
										<option value="7">Běloruština</option>
										<option value="8">Bulharština</option>
										<option value="9">Čeština</option>
										<option value="10">Čínština</option>
										<option value="11">Dánština</option>
										<option value="12">Estonština</option>
										<option value="13">Finština</option>
										<option value="14">Francouzština</option>
										<option value="15">Hebrejština</option>
										<option value="16">Holandština</option>
										<option value="17">Chorvatština</option>
										<option value="18">Indonézština</option>
										<option value="19">Islandština</option>
										<option value="20">Italština</option>
										<option value="21">Japonština</option>
										<option value="22">Katalánština</option>
										<option value="23">Korejština</option>
										<option value="24">Litevština</option>
										<option value="25">Lotyšština</option>
										<option value="26">Maďarština</option>
										<option value="27">Němčina</option>
										<option value="28">Norština</option>
										<option value="29">Polština</option>
										<option value="30">Portugalština</option>
										<option value="31">Rumunština</option>
										<option value="32">Ruština</option>
										<option value="33">Řečtina</option>
										<option value="34">Slovenština</option>
										<option value="35">Slovinština</option>
										<option value="36">Srbština</option>
										<option value="37">Španělština</option>
										<option value="38">Švédština</option>
										<option value="39">Thajština</option>
										<option value="40">Turečtina</option>
										<option value="41">Ukrajinština</option>
										<option value="42">Vietnamština</option>
									</select></label></div>
							<div class="cse-block cse-field-select cse-wrap-langSkill_lvl">
								<label><span class="cse-label">Úroveň<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lvl_2" name="langSkill_lvl[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Úroveňˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Úroveň&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Základní/Pasivní</option>
										<option value="3">Mírně pokročilá</option>
										<option value="4">Středně pokročilá</option>
										<option value="5">Pokročilá</option>
										<option value="6">Výborná/rodilý mluvčí</option>
									</select></label></div>
						</div>
						<div data-number="3" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-langSkill_lang">
								<label><span class="cse-label">Jazyk<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lang_3" name="langSkill_lang[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Jazykˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Jazyk&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Afrikánština</option>
										<option value="3">Albánština</option>
										<option value="4">Angličtina</option>
										<option value="5">Arabština</option>
										<option value="6">Baskičtina</option>
										<option value="7">Běloruština</option>
										<option value="8">Bulharština</option>
										<option value="9">Čeština</option>
										<option value="10">Čínština</option>
										<option value="11">Dánština</option>
										<option value="12">Estonština</option>
										<option value="13">Finština</option>
										<option value="14">Francouzština</option>
										<option value="15">Hebrejština</option>
										<option value="16">Holandština</option>
										<option value="17">Chorvatština</option>
										<option value="18">Indonézština</option>
										<option value="19">Islandština</option>
										<option value="20">Italština</option>
										<option value="21">Japonština</option>
										<option value="22">Katalánština</option>
										<option value="23">Korejština</option>
										<option value="24">Litevština</option>
										<option value="25">Lotyšština</option>
										<option value="26">Maďarština</option>
										<option value="27">Němčina</option>
										<option value="28">Norština</option>
										<option value="29">Polština</option>
										<option value="30">Portugalština</option>
										<option value="31">Rumunština</option>
										<option value="32">Ruština</option>
										<option value="33">Řečtina</option>
										<option value="34">Slovenština</option>
										<option value="35">Slovinština</option>
										<option value="36">Srbština</option>
										<option value="37">Španělština</option>
										<option value="38">Švédština</option>
										<option value="39">Thajština</option>
										<option value="40">Turečtina</option>
										<option value="41">Ukrajinština</option>
										<option value="42">Vietnamština</option>
									</select></label></div>
							<div class="cse-block cse-field-select cse-wrap-langSkill_lvl">
								<label><span class="cse-label">Úroveň<span class="cse-required cse-subrequired"> * </span></span><select id="langSkill_lvl_3" name="langSkill_lvl[]" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮZnalost jazyka - Úroveňˮ je nutné zodpovědět.\&quot;}]}" data-required="required" title="Otázku &quot;Znalost jazyka - Úroveň&quot; je nutné zodpovědět. ">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Základní/Pasivní</option>
										<option value="3">Mírně pokročilá</option>
										<option value="4">Středně pokročilá</option>
										<option value="5">Pokročilá</option>
										<option value="6">Výborná/rodilý mluvčí</option>
									</select></label></div>
						</div>
					</div>
					<div class="cse-block cse-field-textarea cse-wrap-itKnowledge"><label><span class="cse-label">Detailní popis IT znalostí</span><textarea id="itKnowledge" name="itKnowledge" rows="5" cols="40" class="textarea" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;2000\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮDetailní popis IT znalostíˮ můžete zadat maximálně 2000 znaků.\&quot;}]}" maxlength="2000" title="V odpovědi na otázku &quot;Detailní popis IT znalostí&quot; můžete zadat maximálně 2000 znaků. ">
</textarea><span class="cse-hint">Znění nápovědy:Detailní popis IT znalostí</span></label></div>
					<div class="cse-block cse-field-checkbox cse-wrap-drvLicense"><h3>Řidičský průkaz</h3>
						<span class="cse-hint">Znění nápovědy:Řidičský průkaz</span><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_1" value="1" class="checkbox"/> <span class="cse-label">Ne</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_2" value="2" class="checkbox"/> <span class="cse-label">A</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_3" value="3" class="checkbox"/> <span class="cse-label">B</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_4" value="4" class="checkbox"/> <span class="cse-label">C</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_5" value="5" class="checkbox"/> <span class="cse-label">D</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_6" value="6" class="checkbox"/> <span class="cse-label">E</span></label><label><input data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;exclusive\&quot;,\&quot;condition\&quot;:\&quot;1\&quot;,\&quot;message\&quot;:\&quot;Pokud v otázce Řidičský průkaz zvolíte odpověď ˮneˮ, nelze zvolit žádnou další odpověď.\&quot;},{\&quot;type\&quot;:\&quot;maxValues\&quot;,\&quot;condition\&quot;:\&quot;5\&quot;,\&quot;message\&quot;:\&quot;Maximální počet zadaných hodnot v poli Řidičský průkaz je 5.\&quot;}]}" type="checkbox" name="drvLicense[]" id="drvLicense_7" value="7" class="checkbox"/> <span class="cse-label">T</span></label>
					</div>
					<div class="cse-group" data-validation-required="0" data-validation-duplicate="emplType" data-repeatcount="4" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮHledaná práce – typ pr. vztahuˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;emplType\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Hledaná práce – typ pr. vztahu se nesmí opakovat.\&quot;}]}" data-required="required" data-duplicate="emplType" data-title="Otázku &quot;Hledaná práce – typ pr. vztahu&quot; je nutné zodpovědět. Hodnota v poli Hledaná práce – typ pr. vztahu se nesmí opakovat. ">
						<div data-number="0" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-emplType"><label><span class="cse-label">Hledaná práce – typ pr. vztahu</span><select id="emplType_0" name="emplType[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Práce na plný úvazek</option>
										<option value="3">Práce na zkrácený úvazek</option>
										<option value="4">Smluvní zakázky (práce na IČO)</option>
										<option value="5">Brigáda</option>
										<option value="6">Praxe a stáže</option>
										<option value="7">Jiný typ práce</option>
										<option value="8">Trainee programy</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – typ pr. vztahu</span></label>
							</div>
						</div>
						<div data-number="1" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-emplType"><label><span class="cse-label">Hledaná práce – typ pr. vztahu</span><select id="emplType_1" name="emplType[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Práce na plný úvazek</option>
										<option value="3">Práce na zkrácený úvazek</option>
										<option value="4">Smluvní zakázky (práce na IČO)</option>
										<option value="5">Brigáda</option>
										<option value="6">Praxe a stáže</option>
										<option value="7">Jiný typ práce</option>
										<option value="8">Trainee programy</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – typ pr. vztahu</span></label>
							</div>
						</div>
						<div data-number="2" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-emplType"><label><span class="cse-label">Hledaná práce – typ pr. vztahu</span><select id="emplType_2" name="emplType[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Práce na plný úvazek</option>
										<option value="3">Práce na zkrácený úvazek</option>
										<option value="4">Smluvní zakázky (práce na IČO)</option>
										<option value="5">Brigáda</option>
										<option value="6">Praxe a stáže</option>
										<option value="7">Jiný typ práce</option>
										<option value="8">Trainee programy</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – typ pr. vztahu</span></label>
							</div>
						</div>
						<div data-number="3" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-emplType"><label><span class="cse-label">Hledaná práce – typ pr. vztahu</span><select id="emplType_3" name="emplType[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Práce na plný úvazek</option>
										<option value="3">Práce na zkrácený úvazek</option>
										<option value="4">Smluvní zakázky (práce na IČO)</option>
										<option value="5">Brigáda</option>
										<option value="6">Praxe a stáže</option>
										<option value="7">Jiný typ práce</option>
										<option value="8">Trainee programy</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – typ pr. vztahu</span></label>
							</div>
						</div>
						<div data-number="4" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-emplType"><label><span class="cse-label">Hledaná práce – typ pr. vztahu</span><select id="emplType_4" name="emplType[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Práce na plný úvazek</option>
										<option value="3">Práce na zkrácený úvazek</option>
										<option value="4">Smluvní zakázky (práce na IČO)</option>
										<option value="5">Brigáda</option>
										<option value="6">Praxe a stáže</option>
										<option value="7">Jiný typ práce</option>
										<option value="8">Trainee programy</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – typ pr. vztahu</span></label>
							</div>
						</div>
					</div>
					<div class="cse-group" data-validation-required="0" data-validation-duplicate="wrkField" data-repeatcount="2" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮHledaná práce – obor práceˮ je nutné zodpovědět.\&quot;},{\&quot;type\&quot;:\&quot;duplicate\&quot;,\&quot;condition\&quot;:\&quot;wrkField\&quot;,\&quot;message\&quot;:\&quot;Hodnota v poli Hledaná práce – obor práce se nesmí opakovat.\&quot;}]}" data-required="required" data-duplicate="wrkField" data-title="Otázku &quot;Hledaná práce – obor práce&quot; je nutné zodpovědět. Hodnota v poli Hledaná práce – obor práce se nesmí opakovat. ">
						<div data-number="0" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-wrkField"><label><span class="cse-label">Hledaná práce – obor práce</span><select id="wrkField_0" name="wrkField[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Administrativa</option>
										<option value="3">Auto - moto</option>
										<option value="4">Bankovnictví a finanční služby</option>
										<option value="5">Cestovní ruch a ubytování</option>
										<option value="6">Doprava, logistika a zásobování</option>
										<option value="7">Ekonomika a podnikové finance</option>
										<option value="8">Elektrotechnika a energetika</option>
										<option value="9">Farmacie</option>
										<option value="10">Gastronomie a pohostinství</option>
										<option value="11">Chemický průmysl</option>
										<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
										<option value="13">IS/IT: Správa systémů a HW</option>
										<option value="14">IS/IT: Vývoj aplikací a systémů</option>
										<option value="15">Kultura, umění a tvůrčí práce</option>
										<option value="16">Kvalita a kontrola jakosti</option>
										<option value="17">Marketing</option>
										<option value="18">Média, reklama a PR</option>
										<option value="19">Nákup</option>
										<option value="20">Ostraha a bezpečnost</option>
										<option value="21">Personalistika a HR</option>
										<option value="22">Pojišťovnictví</option>
										<option value="23">Potravinářství</option>
										<option value="24">Právní služby</option>
										<option value="25">Prodej a obchod</option>
										<option value="26">Řemeslné a manuální práce</option>
										<option value="27">Služby</option>
										<option value="28">Státní a veřejná správa</option>
										<option value="29">Stavebnictví a reality</option>
										<option value="30">Strojírenství</option>
										<option value="31">Technika a vývoj</option>
										<option value="32">Telekomunikace</option>
										<option value="33">Věda a výzkum</option>
										<option value="34">Vrcholový management</option>
										<option value="35">Vydavatelství, tisk a polygrafie</option>
										<option value="36">Výroba a průmysl</option>
										<option value="37">Vzdělávání a školství</option>
										<option value="38">Zákaznický servis</option>
										<option value="39">Zdravotnictví a sociální péče</option>
										<option value="40">Zemědělství, lesnictví a ekologie</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – obor práce</span></label>
							</div>
						</div>
						<div data-number="1" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-wrkField"><label><span class="cse-label">Hledaná práce – obor práce</span><select id="wrkField_1" name="wrkField[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Administrativa</option>
										<option value="3">Auto - moto</option>
										<option value="4">Bankovnictví a finanční služby</option>
										<option value="5">Cestovní ruch a ubytování</option>
										<option value="6">Doprava, logistika a zásobování</option>
										<option value="7">Ekonomika a podnikové finance</option>
										<option value="8">Elektrotechnika a energetika</option>
										<option value="9">Farmacie</option>
										<option value="10">Gastronomie a pohostinství</option>
										<option value="11">Chemický průmysl</option>
										<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
										<option value="13">IS/IT: Správa systémů a HW</option>
										<option value="14">IS/IT: Vývoj aplikací a systémů</option>
										<option value="15">Kultura, umění a tvůrčí práce</option>
										<option value="16">Kvalita a kontrola jakosti</option>
										<option value="17">Marketing</option>
										<option value="18">Média, reklama a PR</option>
										<option value="19">Nákup</option>
										<option value="20">Ostraha a bezpečnost</option>
										<option value="21">Personalistika a HR</option>
										<option value="22">Pojišťovnictví</option>
										<option value="23">Potravinářství</option>
										<option value="24">Právní služby</option>
										<option value="25">Prodej a obchod</option>
										<option value="26">Řemeslné a manuální práce</option>
										<option value="27">Služby</option>
										<option value="28">Státní a veřejná správa</option>
										<option value="29">Stavebnictví a reality</option>
										<option value="30">Strojírenství</option>
										<option value="31">Technika a vývoj</option>
										<option value="32">Telekomunikace</option>
										<option value="33">Věda a výzkum</option>
										<option value="34">Vrcholový management</option>
										<option value="35">Vydavatelství, tisk a polygrafie</option>
										<option value="36">Výroba a průmysl</option>
										<option value="37">Vzdělávání a školství</option>
										<option value="38">Zákaznický servis</option>
										<option value="39">Zdravotnictví a sociální péče</option>
										<option value="40">Zemědělství, lesnictví a ekologie</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – obor práce</span></label>
							</div>
						</div>
						<div data-number="2" class="cse-panel">
							<div class="cse-block cse-field-select cse-wrap-wrkField"><label><span class="cse-label">Hledaná práce – obor práce</span><select id="wrkField_2" name="wrkField[]" class="select" data-defaultValue="1" title="">
										<option value="1" selected="selected">- vyberte -</option>
										<option value="2">Administrativa</option>
										<option value="3">Auto - moto</option>
										<option value="4">Bankovnictví a finanční služby</option>
										<option value="5">Cestovní ruch a ubytování</option>
										<option value="6">Doprava, logistika a zásobování</option>
										<option value="7">Ekonomika a podnikové finance</option>
										<option value="8">Elektrotechnika a energetika</option>
										<option value="9">Farmacie</option>
										<option value="10">Gastronomie a pohostinství</option>
										<option value="11">Chemický průmysl</option>
										<option value="12">IS/IT: Konzultace, analýzy a projektové řízení</option>
										<option value="13">IS/IT: Správa systémů a HW</option>
										<option value="14">IS/IT: Vývoj aplikací a systémů</option>
										<option value="15">Kultura, umění a tvůrčí práce</option>
										<option value="16">Kvalita a kontrola jakosti</option>
										<option value="17">Marketing</option>
										<option value="18">Média, reklama a PR</option>
										<option value="19">Nákup</option>
										<option value="20">Ostraha a bezpečnost</option>
										<option value="21">Personalistika a HR</option>
										<option value="22">Pojišťovnictví</option>
										<option value="23">Potravinářství</option>
										<option value="24">Právní služby</option>
										<option value="25">Prodej a obchod</option>
										<option value="26">Řemeslné a manuální práce</option>
										<option value="27">Služby</option>
										<option value="28">Státní a veřejná správa</option>
										<option value="29">Stavebnictví a reality</option>
										<option value="30">Strojírenství</option>
										<option value="31">Technika a vývoj</option>
										<option value="32">Telekomunikace</option>
										<option value="33">Věda a výzkum</option>
										<option value="34">Vrcholový management</option>
										<option value="35">Vydavatelství, tisk a polygrafie</option>
										<option value="36">Výroba a průmysl</option>
										<option value="37">Vzdělávání a školství</option>
										<option value="38">Zákaznický servis</option>
										<option value="39">Zdravotnictví a sociální péče</option>
										<option value="40">Zemědělství, lesnictví a ekologie</option>
									</select><span class="cse-hint">Znění nápovědy:Hledaná práce – obor práce</span></label>
							</div>
						</div>
					</div>
					<div class="cse-block cse-field-locality cse-wrap-wrkLocation"><label><span class="cse-label">Hledaná práce – lokalita</span><select id="wrkLocation" name="wrkLocation" title="">
								<option value="-1" data-parent="" selected="selected">- vyberte -</option>
								<option value="C2" data-parent="">Albánie</option>
								<option value="C5" data-parent="">Andorra</option>
								<option value="C24" data-parent="">Belgie</option>
								<option value="C23" data-parent="">Bělorusko</option>
								<option value="C30" data-parent="">Bosna a Hercegovina</option>
								<option value="C37" data-parent="">Bulharsko</option>
								<option value="R190300" data-parent="C37">   Blagoevgrad kraj - BG</option>
								<option value="R190301" data-parent="C37">   Burgas kraj - BG</option>
								<option value="R190302" data-parent="C37">   Dobrich kraj - BG</option>
								<option value="R190303" data-parent="C37">   Gabrovo kraj - BG</option>
								<option value="R190304" data-parent="C37">   Haskovo kraj - BG</option>
								<option value="R190305" data-parent="C37">   Kardzhali kraj - BG</option>
								<option value="R190306" data-parent="C37">   Kyustendil kraj - BG</option>
								<option value="R190307" data-parent="C37">   Lovech kraj - BG</option>
								<option value="R190308" data-parent="C37">   Montana kraj - BG</option>
								<option value="R190309" data-parent="C37">   Pazardzhik kraj - BG</option>
								<option value="R190310" data-parent="C37">   Pernik kraj - BG</option>
								<option value="R190311" data-parent="C37">   Pleven kraj - BG</option>
								<option value="R190312" data-parent="C37">   Plovdiv kraj - BG</option>
								<option value="R190313" data-parent="C37">   Razgrad kraj - BG</option>
								<option value="R190314" data-parent="C37">   Ruse kraj - BG</option>
								<option value="R190315" data-parent="C37">   Shumen kraj - BG</option>
								<option value="R190316" data-parent="C37">   Silistra kraj - BG</option>
								<option value="R190317" data-parent="C37">   Sliven kraj - BG</option>
								<option value="R190318" data-parent="C37">   Smolyan kraj - BG</option>
								<option value="R190319" data-parent="C37">   Sofia kraj - BG</option>
								<option value="R190320" data-parent="C37">   Sofia Province kraj - BG</option>
								<option value="R190321" data-parent="C37">   Stara Zagora kraj - BG</option>
								<option value="R190322" data-parent="C37">   Targovishte kraj - BG</option>
								<option value="R190323" data-parent="C37">   Varna kraj - BG</option>
								<option value="R190324" data-parent="C37">   Veliko Tarnovo kraj - BG</option>
								<option value="R190325" data-parent="C37">   Vidin kraj - BG</option>
								<option value="R190326" data-parent="C37">   Vratsa kraj - BG</option>
								<option value="R190327" data-parent="C37">   Yambol kraj - BG</option>
								<option value="C56" data-parent="">Česká republika</option>
								<option value="R200000" data-parent="C56">   Hlavní město Praha - CZ</option>
								<option value="R223500" data-parent="C56">   Jihočeský kraj - CZ</option>
								<option value="R264745" data-parent="C56">   Jihomoravský kraj - CZ</option>
								<option value="R235864" data-parent="C56">   Karlovarský kraj - CZ</option>
								<option value="R259737" data-parent="C56">   kraj Vysočina - CZ</option>
								<option value="R249811" data-parent="C56">   Královéhradecký kraj - CZ</option>
								<option value="R245749" data-parent="C56">   Liberecký kraj - CZ</option>
								<option value="R277469" data-parent="C56">   Moravskoslezský kraj - CZ</option>
								<option value="R272610" data-parent="C56">   Olomoucký kraj - CZ</option>
								<option value="R255276" data-parent="C56">   Pardubický kraj - CZ</option>
								<option value="R230064" data-parent="C56">   Plzeňský kraj - CZ</option>
								<option value="R206696" data-parent="C56">   Středočeský kraj - CZ</option>
								<option value="R238671" data-parent="C56">   Ústecký kraj - CZ</option>
								<option value="R285761" data-parent="C56">   Zlínský kraj - CZ</option>
								<option value="C65" data-parent="">Dánsko</option>
								<option value="C75" data-parent="">Estonsko</option>
								<option value="R310362" data-parent="C75">   Harju County kraj - EE</option>
								<option value="R310365" data-parent="C75">   Hiiu County kraj - EE</option>
								<option value="R310363" data-parent="C75">   Ida-Viru County kraj - EE</option>
								<option value="R310368" data-parent="C75">   Järva County kraj - EE</option>
								<option value="R310367" data-parent="C75">   Jõgeva County kraj - EE</option>
								<option value="R310366" data-parent="C75">   Lääne County kraj - EE</option>
								<option value="R310371" data-parent="C75">   Lääne-Viru County kraj - EE</option>
								<option value="R310364" data-parent="C75">   Pärnu County kraj - EE</option>
								<option value="R310372" data-parent="C75">   Põlva County kraj - EE</option>
								<option value="R310369" data-parent="C75">   Rapla County kraj - EE</option>
								<option value="R310370" data-parent="C75">   Saaremaa kraj - EE</option>
								<option value="R310361" data-parent="C75">   Tartu County kraj - EE</option>
								<option value="R310374" data-parent="C75">   Valga County kraj - EE</option>
								<option value="R310375" data-parent="C75">   Viljandi County kraj - EE</option>
								<option value="R310373" data-parent="C75">   Võru County kraj - EE</option>
								<option value="C81" data-parent="">Finsko</option>
								<option value="C82" data-parent="">Francie</option>
								<option value="C61" data-parent="">Chorvatsko</option>
								<option value="C117" data-parent="">Irsko</option>
								<option value="C112" data-parent="">Island</option>
								<option value="C119" data-parent="">Itálie</option>
								<option value="C142" data-parent="">Lichtenštejnsko</option>
								<option value="C143" data-parent="">Litva</option>
								<option value="R310452" data-parent="C143">   Alytus County kraj - LT</option>
								<option value="R310454" data-parent="C143">   Kaunas County kraj - LT</option>
								<option value="R310447" data-parent="C143">   Klaipėda County kraj - LT</option>
								<option value="R310449" data-parent="C143">   Marijampolė County kraj - LT</option>
								<option value="R310451" data-parent="C143">   Panevėžys County kraj - LT</option>
								<option value="R310455" data-parent="C143">   Šiauliai County kraj - LT</option>
								<option value="R310446" data-parent="C143">   Tauragė County kraj - LT</option>
								<option value="R310450" data-parent="C143">   Telšiai County kraj - LT</option>
								<option value="R310453" data-parent="C143">   Utena County kraj - LT</option>
								<option value="R310448" data-parent="C143">   Vilnius County kraj - LT</option>
								<option value="C137" data-parent="">Lotyšsko</option>
								<option value="R310386" data-parent="C137">   Kurzeme kraj - LV</option>
								<option value="R310388" data-parent="C137">   Latgale kraj - LV</option>
								<option value="R310389" data-parent="C137">   Vidzeme kraj - LV</option>
								<option value="R310387" data-parent="C137">   Zemgale kraj - LV</option>
								<option value="C144" data-parent="">Lucembursko</option>
								<option value="C111" data-parent="">Maďarsko</option>
								<option value="R190200" data-parent="C111">   Bács-Kiskun megye kraj - HU</option>
								<option value="R190201" data-parent="C111">   Baranya megye kraj - HU</option>
								<option value="R190202" data-parent="C111">   Békés megye kraj - HU</option>
								<option value="R190203" data-parent="C111">   Borsod-Abaúj-Zemplén megye kraj - HU
								</option>
								<option value="R190219" data-parent="C111">   Budapest és környéke kraj - HU</option>
								<option value="R190204" data-parent="C111">   Csongrád megye kraj - HU</option>
								<option value="R190205" data-parent="C111">   Fejér megye kraj - HU</option>
								<option value="R190206" data-parent="C111">   Gyõr-Moson-Sopron megye kraj - HU</option>
								<option value="R190207" data-parent="C111">   Hajdú-Bihar megye kraj - HU</option>
								<option value="R190208" data-parent="C111">   Heves megye kraj - HU</option>
								<option value="R190209" data-parent="C111">   Jász-Nagykun-Szolnok megye kraj - HU
								</option>
								<option value="R190210" data-parent="C111">   Komárom-Esztergom megye kraj - HU</option>
								<option value="R190211" data-parent="C111">   Nógrád megye kraj - HU</option>
								<option value="R190212" data-parent="C111">   Pest megye kraj - HU</option>
								<option value="R190213" data-parent="C111">   Somogy megye kraj - HU</option>
								<option value="R190214" data-parent="C111">   Szabolcs-Szatmár-Bereg megye kraj - HU
								</option>
								<option value="R190215" data-parent="C111">   Tolna megye kraj - HU</option>
								<option value="R190216" data-parent="C111">   Vas megye kraj - HU</option>
								<option value="R190217" data-parent="C111">   Veszprém megye kraj - HU</option>
								<option value="R190218" data-parent="C111">   Zala megye kraj - HU</option>
								<option value="C146" data-parent="">Makedonská republika</option>
								<option value="C152" data-parent="">Malta</option>
								<option value="C163" data-parent="">Moldavsko</option>
								<option value="R310538" data-parent="C163">   Anenii Noi kraj - MD</option>
								<option value="R310537" data-parent="C163">   Bălţi kraj - MD</option>
								<option value="R310540" data-parent="C163">   Basarabeasca kraj - MD</option>
								<option value="R310539" data-parent="C163">   Briceni kraj - MD</option>
								<option value="R310541" data-parent="C163">   Cahul kraj - MD</option>
								<option value="R310543" data-parent="C163">   Călăraşi kraj - MD</option>
								<option value="R310531" data-parent="C163">   Cantemir kraj - MD</option>
								<option value="R310532" data-parent="C163">   Căuşeni kraj - MD</option>
								<option value="R310544" data-parent="C163">   Căuşeni kraj - MD</option>
								<option value="R310530" data-parent="C163">   Cimişlia kraj - MD</option>
								<option value="R310535" data-parent="C163">   Criuleni kraj - MD</option>
								<option value="R310536" data-parent="C163">   Donduşeni kraj - MD</option>
								<option value="R310533" data-parent="C163">   Drochia kraj - MD</option>
								<option value="R310534" data-parent="C163">   Dubăsari kraj - MD</option>
								<option value="R310523" data-parent="C163">   Edineţ kraj - MD</option>
								<option value="R310524" data-parent="C163">   Făleşti kraj - MD</option>
								<option value="R310521" data-parent="C163">   Floreşti kraj - MD</option>
								<option value="R310529" data-parent="C163">   Gagauzia kraj - MD</option>
								<option value="R310522" data-parent="C163">   Glodeni kraj - MD</option>
								<option value="R310527" data-parent="C163">   Hânceşti kraj - MD</option>
								<option value="R310542" data-parent="C163">   Chişinău kraj - MD</option>
								<option value="R310528" data-parent="C163">   Ialoveni kraj - MD</option>
								<option value="R310525" data-parent="C163">   Leova kraj - MD</option>
								<option value="R310526" data-parent="C163">   Nisporeni kraj - MD</option>
								<option value="R310516" data-parent="C163">   Ocniţa kraj - MD</option>
								<option value="R310515" data-parent="C163">   Orhei kraj - MD</option>
								<option value="R310514" data-parent="C163">   Rezina kraj - MD</option>
								<option value="R310513" data-parent="C163">   Rîşcani kraj - MD</option>
								<option value="R310520" data-parent="C163">   Singerei kraj - MD</option>
								<option value="R310519" data-parent="C163">   Şoldăneşti kraj - MD</option>
								<option value="R310518" data-parent="C163">   Soroca kraj - MD</option>
								<option value="R310517" data-parent="C163">   Ştefan Vodă kraj - MD</option>
								<option value="R310511" data-parent="C163">   Străşeni kraj - MD</option>
								<option value="R310510" data-parent="C163">   Taraclia kraj - MD</option>
								<option value="R310509" data-parent="C163">   Teleneşti kraj - MD</option>
								<option value="R310508" data-parent="C163">   Transnistria kraj - MD</option>
								<option value="R310512" data-parent="C163">   Ungheni kraj - MD</option>
								<option value="C164" data-parent="">Monako</option>
								<option value="C91" data-parent="">Německo</option>
								<option value="C174" data-parent="">Nizozemí</option>
								<option value="C184" data-parent="">Norsko</option>
								<option value="C196" data-parent="">Polsko</option>
								<option value="R190100" data-parent="C196">   dolnośląskie kraj - PL</option>
								<option value="R190101" data-parent="C196">   kujawsko-pomorskie kraj - PL</option>
								<option value="R190104" data-parent="C196">   łódzkie kraj - PL</option>
								<option value="R190102" data-parent="C196">   lubelskie kraj - PL</option>
								<option value="R190103" data-parent="C196">   lubuskie kraj - PL</option>
								<option value="R190105" data-parent="C196">   małopolskie kraj - PL</option>
								<option value="R190106" data-parent="C196">   mazowieckie kraj - PL</option>
								<option value="R190107" data-parent="C196">   opolskie kraj - PL</option>
								<option value="R190108" data-parent="C196">   podkarpackie kraj - PL</option>
								<option value="R190109" data-parent="C196">   podlaskie kraj - PL</option>
								<option value="R190110" data-parent="C196">   pomorskie kraj - PL</option>
								<option value="R190111" data-parent="C196">   śląskie kraj - PL</option>
								<option value="R190112" data-parent="C196">   świętokrzyskie kraj - PL</option>
								<option value="R190113" data-parent="C196">   warmińsko-mazurskie kraj - PL</option>
								<option value="R190114" data-parent="C196">   wielkopolskie kraj - PL</option>
								<option value="R190115" data-parent="C196">   zachodniopomorskie kraj - PL</option>
								<option value="C197" data-parent="">Portugalsko</option>
								<option value="C15" data-parent="">Rakousko</option>
								<option value="C201" data-parent="">Rumunsko</option>
								<option value="R190400" data-parent="C201">   Alba kraj - RO</option>
								<option value="R190401" data-parent="C201">   Arad kraj - RO</option>
								<option value="R190402" data-parent="C201">   Argeş kraj - RO</option>
								<option value="R190403" data-parent="C201">   Bacău kraj - RO</option>
								<option value="R190404" data-parent="C201">   Bihor kraj - RO</option>
								<option value="R190405" data-parent="C201">   Bistriţa-Năsăud kraj - RO</option>
								<option value="R190406" data-parent="C201">   Botoşani kraj - RO</option>
								<option value="R190408" data-parent="C201">   Brăila kraj - RO</option>
								<option value="R190407" data-parent="C201">   Braşov kraj - RO</option>
								<option value="R190441" data-parent="C201">   Bucureşti kraj - RO</option>
								<option value="R190409" data-parent="C201">   Buzău kraj - RO</option>
								<option value="R190414" data-parent="C201">   Călăraşi kraj - RO</option>
								<option value="R190410" data-parent="C201">   Caraş-Severin kraj - RO</option>
								<option value="R190411" data-parent="C201">   Cluj kraj - RO</option>
								<option value="R190412" data-parent="C201">   Constanţa kraj - RO</option>
								<option value="R190413" data-parent="C201">   Covasna kraj - RO</option>
								<option value="R190416" data-parent="C201">   Dâmboviţa kraj - RO</option>
								<option value="R190415" data-parent="C201">   Dolj kraj - RO</option>
								<option value="R190417" data-parent="C201">   Galaţi kraj - RO</option>
								<option value="R190418" data-parent="C201">   Giurgiu kraj - RO</option>
								<option value="R190419" data-parent="C201">   Gorj kraj - RO</option>
								<option value="R190420" data-parent="C201">   Harghita kraj - RO</option>
								<option value="R190421" data-parent="C201">   Hunedoara kraj - RO</option>
								<option value="R190422" data-parent="C201">   Ialomiţa kraj - RO</option>
								<option value="R190423" data-parent="C201">   Iaşi kraj - RO</option>
								<option value="R190424" data-parent="C201">   Ilfov kraj - RO</option>
								<option value="R190425" data-parent="C201">   Maramureş kraj - RO</option>
								<option value="R190426" data-parent="C201">   Mehedinţi kraj - RO</option>
								<option value="R190427" data-parent="C201">   Mureş kraj - RO</option>
								<option value="R190428" data-parent="C201">   Neamţ kraj - RO</option>
								<option value="R190429" data-parent="C201">   Olt kraj - RO</option>
								<option value="R190430" data-parent="C201">   Prahova kraj - RO</option>
								<option value="R190434" data-parent="C201">   Sălaj kraj - RO</option>
								<option value="R190431" data-parent="C201">   SatuMare kraj - RO</option>
								<option value="R190432" data-parent="C201">   Sibiu kraj - RO</option>
								<option value="R190433" data-parent="C201">   Suceava kraj - RO</option>
								<option value="R190435" data-parent="C201">   Teleorman kraj - RO</option>
								<option value="R190436" data-parent="C201">   Timiş kraj - RO</option>
								<option value="R190437" data-parent="C201">   Tulcea kraj - RO</option>
								<option value="R190440" data-parent="C201">   Vâlcea kraj - RO</option>
								<option value="R190438" data-parent="C201">   Vaslui kraj - RO</option>
								<option value="R190439" data-parent="C201">   Vrancea kraj - RO</option>
								<option value="C202" data-parent="">Rusko (Evropská část)</option>
								<option value="C95" data-parent="">Řecko</option>
								<option value="C210" data-parent="">San Marino</option>
								<option value="C217" data-parent="">Slovensko</option>
								<option value="R190001" data-parent="C217">   Banskobystrický kraj - SK</option>
								<option value="R190000" data-parent="C217">   Bratislavský kraj - SK</option>
								<option value="R190007" data-parent="C217">   Košický kraj - SK</option>
								<option value="R190005" data-parent="C217">   Nitriansky kraj - SK</option>
								<option value="R190006" data-parent="C217">   Prešovský kraj - SK</option>
								<option value="R190003" data-parent="C217">   Trenčiansky kraj - SK</option>
								<option value="R190004" data-parent="C217">   Trnavský kraj - SK</option>
								<option value="R190002" data-parent="C217">   Žilinský kraj - SK</option>
								<option value="C218" data-parent="">Slovinsko</option>
								<option value="C267" data-parent="">Srbsko a Černá Hora</option>
								<option value="R310621" data-parent="C267">   Central Serbia kraj - YU</option>
								<option value="R310623" data-parent="C267">   Kosovo and Metohija kraj - YU</option>
								<option value="R310622" data-parent="C267">   Vojvodina kraj - YU</option>
								<option value="C223" data-parent="">Španělsko</option>
								<option value="C230" data-parent="">Švédsko</option>
								<option value="C231" data-parent="">Švýcarsko</option>
								<option value="C248" data-parent="">Ukrajina</option>
								<option value="R310756" data-parent="C248">   Autonomous Republic of Crimea kraj - UA
								</option>
								<option value="R310773" data-parent="C248">   Dnipropetrovsk Oblast kraj - UA</option>
								<option value="R310780" data-parent="C248">   Donetsk Oblast kraj - UA</option>
								<option value="R310776" data-parent="C248">   Cherkasy Oblast kraj - UA</option>
								<option value="R310775" data-parent="C248">   Chernihiv Oblast kraj - UA</option>
								<option value="R310774" data-parent="C248">   Chernivtsi Oblast kraj - UA</option>
								<option value="R310768" data-parent="C248">   Ivano-Frankivsk Oblast kraj - UA</option>
								<option value="R310779" data-parent="C248">   Kharkiv Oblast kraj - UA</option>
								<option value="R310778" data-parent="C248">   Kherson Oblast kraj - UA</option>
								<option value="R310777" data-parent="C248">   Khmelnytskyi Oblast kraj - UA</option>
								<option value="R310766" data-parent="C248">   Kiev Oblast kraj - UA</option>
								<option value="R310767" data-parent="C248">   Kirovohrad Oblast kraj - UA</option>
								<option value="R310765" data-parent="C248">   Luhansk Oblast kraj - UA</option>
								<option value="R310771" data-parent="C248">   Lviv Oblast kraj - UA</option>
								<option value="R310770" data-parent="C248">   Mykolaiv Oblast kraj - UA</option>
								<option value="R310769" data-parent="C248">   Odessa Oblast kraj - UA</option>
								<option value="R310759" data-parent="C248">   Poltava Oblast kraj - UA</option>
								<option value="R310760" data-parent="C248">   Rivne Oblast kraj - UA</option>
								<option value="R310757" data-parent="C248">   Sumy Oblast kraj - UA</option>
								<option value="R310758" data-parent="C248">   Ternopil Oblast kraj - UA</option>
								<option value="R310764" data-parent="C248">   Vinnytsia Oblast kraj - UA</option>
								<option value="R310772" data-parent="C248">   Volyn Oblast kraj - UA</option>
								<option value="R310763" data-parent="C248">   Zakarpattia Oblast kraj - UA</option>
								<option value="R310761" data-parent="C248">   Zaporizhia Oblast kraj - UA</option>
								<option value="R310762" data-parent="C248">   Zhytomyr Oblast kraj - UA</option>
								<option value="C107" data-parent="">Vatikán</option>
								<option value="C250" data-parent="">Velká Británie a Severní Irsko</option>
							</select><span class="cse-hint">Znění nápovědy:Hledaná práce – lokalita</span></label></div>
					<div class="cse-block cse-field-select cse-wrap-salary"><label><span class="cse-label">Požadovaný plat</span><select id="salary" name="salary" class="select" data-defaultValue="1" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;match_js\&quot;,\&quot;condition\&quot;:\&quot;(^$)|(^\d{0,6}$)\&quot;,\&quot;message\&quot;:\&quot;Odpověď na otázku ˮPožadovaný platˮ zadejte ve správném formátu.\&quot;}]}" title="Odpověď na otázku &quot;Požadovaný plat&quot; zadejte ve správném formátu.">
								<option value="1" selected="selected">- vyberte -</option>
								<option value="2">5000 - 10000</option>
								<option value="3">11000 - 15000</option>
								<option value="4">16000 - 20000</option>
								<option value="5">21000 - 30000</option>
							</select><span class="cse-hint">Znění nápovědy:Požadovaný plat</span></label></div>
					<div class="cse-free-text"> Volný text interdum tempor eros, non semper purus commodo sed? Curabitur
						sit amet condimentum velit posuere. In interdum tempor eros, non semper purus commodo sed?
						Curabitur sit amet condimentum velit posuere.<br/> <br/></div>
					<div class="cse-block cse-field-select cse-wrap-hrDate"><label><span class="cse-label">Datum možného nástupu</span><select id="hrDate" name="hrDate" class="select" data-defaultValue="1" title="">
								<option value="1" selected="selected">- vyberte -</option>
								<option value="2">Ihned</option>
								<option value="3">1 měsíc</option>
								<option value="4">2 měsíce</option>
								<option value="5">3 měsíce</option>
							</select><span class="cse-hint">Znění nápovědy:Datum možného nástupu</span></label></div>
					<div class="cse-block cse-field-cv cse-wrap-cv">
						<label><span class="cse-label">Příloha – životopis</span><input type="file" id="cv" name="cv" class="file" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;fileSize\&quot;,\&quot;condition\&quot;:\&quot;800000\&quot;,\&quot;message\&quot;:\&quot;Přiložený soubor v otázce ˮPříloha – životopisˮ musí mít maximální velikost 800 kB.\&quot;}]}" title=""/><span class="cse-hint">Znění nápovědy:Příloha – životopis</span></label>
					</div>
					<div class="cse-group" data-validation-required="0" data-repeatcount="2" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;required\&quot;,\&quot;condition\&quot;:\&quot;0\&quot;,\&quot;message\&quot;:\&quot;Otázku ˮPříloha – ostatníˮ je nutné zodpovědět.\&quot;}]}" data-required="required" data-title="Otázku &quot;Příloha – ostatní&quot; je nutné zodpovědět. ">
						<div data-number="0" class="cse-panel">
							<div class="cse-block cse-field-file cse-wrap-attachment"><label><span class="cse-label">Příloha – ostatní</span><input type="file" id="attachment_0" name="attachment[]" class="file" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;fileSize\&quot;,\&quot;condition\&quot;:\&quot;800000\&quot;,\&quot;message\&quot;:\&quot;Přiložený soubor v otázce ˮPříloha – ostatníˮ musí mít maximální velikost 800 kB.\&quot;}]}" title=""/><span class="cse-hint">Znění nápovědy:Příloha – ostatní</span></label>
							</div>
						</div>
						<div data-number="1" class="cse-panel">
							<div class="cse-block cse-field-file cse-wrap-attachment"><label><span class="cse-label">Příloha – ostatní</span><input type="file" id="attachment_1" name="attachment[]" class="file" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;fileSize\&quot;,\&quot;condition\&quot;:\&quot;800000\&quot;,\&quot;message\&quot;:\&quot;Přiložený soubor v otázce ˮPříloha – ostatníˮ musí mít maximální velikost 800 kB.\&quot;}]}" title=""/><span class="cse-hint">Znění nápovědy:Příloha – ostatní</span></label>
							</div>
						</div>
						<div data-number="2" class="cse-panel">
							<div class="cse-block cse-field-file cse-wrap-attachment"><label><span class="cse-label">Příloha – ostatní</span><input type="file" id="attachment_2" name="attachment[]" class="file" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;fileSize\&quot;,\&quot;condition\&quot;:\&quot;800000\&quot;,\&quot;message\&quot;:\&quot;Přiložený soubor v otázce ˮPříloha – ostatníˮ musí mít maximální velikost 800 kB.\&quot;}]}" title=""/><span class="cse-hint">Znění nápovědy:Příloha – ostatní</span></label>
							</div>
						</div>
					</div>
					<div class="cse-block cse-field-textarea cse-wrap-text"><label><span class="cse-label">Motivační dopis</span><textarea id="text" name="text" rows="5" cols="40" class="textarea" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;2000\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮMotivační dopisˮ můžete zadat maximálně 2000 znaků.\&quot;}]}" maxlength="2000" title="V odpovědi na otázku &quot;Motivační dopis&quot; můžete zadat maximálně 2000 znaků. ">
</textarea><span class="cse-hint">Znění nápovědy:Motivační dopis</span></label></div>
					<h2 class="cse-separator-hidden">Separator - Odsazení</h2>
					<hr class="cse-line"/>
					<div class="cse-block cse-field-text cse-wrap-c1317800138072"><label><span class="cse-label">Vlastní – otázka</span><input type="text" id="c1317800138072" name="c1317800138072" class="text" value="" data-validations="{\&quot;validations\&quot;:[{\&quot;type\&quot;:\&quot;maxlength\&quot;,\&quot;condition\&quot;:\&quot;50\&quot;,\&quot;message\&quot;:\&quot;V odpovědi na otázku ˮVlastní – otázkaˮ můžete zadat maximálně 50 znaků.\&quot;}]}" maxlength="50" title="V odpovědi na otázku &quot;Vlastní – otázka&quot; můžete zadat maximálně 50 znaků. "/><span class="cse-hint">Znění nápovědy:Vlastní – otázka</span></label>
					</div>
				</div>
				<div class="cse-block cse-field-checkbox cse-wrap-accept_101 cse-wrap-lmc-consent">
					<label for="lmc-consent"><input id="lmc-consent" name="lmc-consent-lmc" type="checkbox" value="1"><span class="cse-label">Souhlasím, aby mi provozovatel [Jobs.cz] (LMC s.r.o.) pomáhal k nalezení lepší práce a osobnímu rozvoji. <span class="cse-label-gdpr">Zobrazit více</span></span></label>
					<div class="cse-consent-content" style="display: none;">Chci zvýšit své šance na uplatnění v
						budoucnu a mám zájem, aby mi společnost LMC s.r.o., IČO: 264 41 381 poskytla služby usnadňující
						výběr vhodné pracovní příležitosti a další služby z řad vlastních produktů týkajících se
						osobního rozvoje, vzdělávání, hodnocení zaměstnavatelů a dalších, které mohou zlepšit mé
						postavení na trhu práce, zvýšit šance na kvalitní zaměstnání nebo jsou jinak spojené s trhem
						práce. Z těchto důvodů souhlasím se zpracováním osobních údajů pro účely evidování mé osoby. Za
						tímto účelem společnost posuzuje hlediska, které pomůžou personalizovat obsah nabídek (informace
						vyplněné v odpovědních formulářích, uvedené v životopisech či získané vyhodnocováním výběrových
						řízení, kterých se účastním prostřednictvím platforem společnosti LMC). Informace o zpracování
						údajů poskytuje pověřenec pro ochranu osobních údajů Jan Svoboda, e-mail: dpo@lmc.eu. Souhlas
						uděluji na dobu 10 let. Souhlas mohu kdykoliv odvolat.
					</div>
				</div>
				<div class="cse-block cse-field-checkbox cse-wrap-accept_101">
					<label for="accept_1011"><input id="accept_1011" name="accept_101" type="checkbox" value="1"><span class="cse-label">Souhlas se zpracováním osobních údajů. <span class="cse-label-gdpr">Zobrazit více</span></span></label>
					<div class="cse-consent-content" style="display: none;">Souhlasím se zpracováním osobních údajů.
						Zpracování osobních údajů upravuje zákon č. 101/2000 Sb., o ochraně osobních údajů. Souhlas je
						udělen společnosti Test Company s.r.o. jakožto správci pro účely výběrového řízení na danou
						pracovní pozici a dále pro účely evidování mé osoby jakožto potenciálního zaměstnance u této
						společnosti. Souhlas uděluji na dobu 3 roky ode dne odeslání osobních údajů. Beru na vědomí, že
						udělení souhlasu je dobrovolné a že jej mohu kdykoli odvolat. Jsem srozuměn s tím, že zpracování
						osobních údajů bude provádět společnost LMC s.r.o., IČ: 264 41&nbsp;381, se sídlem Praha 7,
						Jankovcova 1569/2c, PSČ 170 00, prostřednictvím svých elektronických systémů. Jsem informován o
						tom, že mám právo na informaci o tom, jaké osobní údaje, k&nbsp;jakému účelu a jakým způsobem
						jsou o mně zpracovávány. Pokud zjistím nebo se domnívám, že správce provádí zpracování mých
						osobních údajů, které je v rozporu s&nbsp;ochranou mého soukromého a osobního života nebo v
						rozporu se zákonem, zejména jsou-li osobní údaje nepřesné s ohledem na účel jejich zpracování,
						mohu požádat správce o vysvětlení, nebo požadovat, aby správce odstranil takto vzniklý stav.
						Zejména se může jednat o blokování, provedení opravy, doplnění nebo likvidaci osobních údajů.
					</div>
				</div>
				<div class="cse-block cse-field-checkbox cse-wrap-accept_101 cse-wrap-lmc-consent">
					<div class="cse-job-board-consent-content">Odesláním odpovědi souhlasíte s
						<a href="https://www.lmc.eu/cs/podminky-pouzivani-sluzeb/specificke-podminky/jobs+prace" target="_blank">podmínkami
							používání Prace.cz</a></div>
				</div>
				<div class="cse-block cse-field-submit cse-wrap-flexi-submit">
					<div>
						<input value="1" type="hidden" name="sended"/><input value="flexi_1317800056069" type="hidden" name="reportId"/><input value="240822803" type="hidden" name="advId"/><input value="g2" type="hidden" name="brand"/><input value="cs" type="hidden" name="lang"/><input value="cz" type="hidden" name="langgui" id="langgui"/><input value="186" type="hidden" name="rps"/>
						<script type="text/javascript">
                            var opInfoDot = "o200900005;p201100029";
						</script>
					</div>
					<div class="cse-block cse-field-submit cse-buttons">
						<input value="Odeslat" type="submit" name="flexi-submit" id="flexi-submit"/></div>
				</div>
			</fieldset>
			<p class="cse-need"><em><span class="cse-required">*</span> Pro úspěšné odeslání formuláře je potřeba
					vyplnit všechna pole označená
					hvězdičkou.</em><br/><em><span class="cse-required cse-subrequired">* </span>Při vyplnění alespoň
					jedné položky z označeného pole se označené položky stávají povinné.</em></p></form>
		<p class="cse-footer">Zápatí interdum tempor eros, non semper purus commodo sed? Curabitur sit amet condimentum
			velit posuere. In interdum tempor eros, non semper purus commodo sed? Curabitur sit amet condimentum velit
			posuere.<br/></p></div>
	<script type="text/javascript" src="//presentation.lmc.cz/cse_templates/js/cse-validate.js<?= \CD\GlobalVersion::get(); ?>"><!----></script>

<?php } ?>
