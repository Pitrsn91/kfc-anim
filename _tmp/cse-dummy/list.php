<?php if (isset($_GET['empty'])) { ?>

	<div class="cse-cont cse-list-wrap">
		<p class="cse-message">Aktuálně nenabízíme žádné volné pozice</p>
	</div>

<?php } else { ?>

<div class="cse-cont cse-list-wrap">
	<ul>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="Oblastní manažer gastroprovozoven v Praze">Oblastní manažer gastroprovozoven v Praze</a></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="Pracovník oddělení údržby/ Technik/ Elektrikář">Pracovník oddělení údržby/ Technik/ Elektrikář</a><span class="cse-icon cse-graduate" title="Pozice je vhodná pro absolventy">&#160;</span></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="MANAGER RESTAURANT CONCEPTS AIRPORT PRAGUE">MANAGER RESTAURANT CONCEPTS AIRPORT PRAGUE</a></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="Software Developer - (C#, .NET, WPF, Winforms)">Software Developer - (C#, .NET, WPF, Winforms)</a><span class="cse-icon cse-graduate" title="Pozice je vhodná pro absolventy">&#160;</span><span class="cse-icon cse-handicap" title="Pozice vhodná pro OZP">&#160;</span></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="Provozní manažer music baru">Provozní manažer music baru</a><span class="cse-icon cse-handicap" title="Pozice vhodná pro OZP">&#160;</span></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="VÝKONNÝ ŘEDITEL/KA">VÝKONNÝ ŘEDITEL/KA</a></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="Manažer obchodního týmu - region Praha a střední Čechy">Manažer obchodního týmu - region Praha a střední Čechy</a></li>
		<li><a href="<?php echo get_permalink('detail-pozice'); ?>" title="DISTRICT MANAGER pro oblast Čechy - catering a gastronomie">DISTRICT MANAGER pro oblast Čechy - catering a gastronomie</a></li>
	</ul>

	<div id="cse-paging">
		<span class="cse-previous">Předchozí strana</span>
		<span class="cse-paging cse-links"><strong>1</strong>&nbsp;<a href="#">2</a>&nbsp;<a href="#">3</a>&nbsp;<a href="#">4</a>&nbsp;<a href="#">5</a>&nbsp;</span>
		<a class="cse-next" href="#" title="Další strana">Další strana</a>
		<hr class="cse-divider">
		<em class="cse-paging cse-info">Strana&nbsp;1&nbsp;z&nbsp;7</em>
	</div>
</div>

<?php } ?>
