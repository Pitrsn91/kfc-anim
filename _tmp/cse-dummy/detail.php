<?php if (isset($_GET['invalid'])) { ?>

	<div class="cse-cont cse-detail-wrap">
		<h3>Tato pozice již neplatí!</h3>
		<p>Je nám líto, ale požadovaná pozice již není platná - byla zaměstnavatelem stažena. Zkuste prosím příště odpovědět na pozici dříve.</p>
	</div>

<?php } else { ?>

<div class="cse-cont cse-detail-wrap">
	<script>if(document.getElementById('cse-gtm')){LMCdataLayer.push({'event':'showDetail','ecommerce':{'detail':{'products':[{'name':'Název pozice 3 CZ','id':'G2-856421690-aden_brand0','price':'1.00','brand':'csetemplates 1700316041','category':'JD/Bankovnictví a finanční služby/Auditor','variant':'KS'}]}}});}</script>
	<div class="cse-block" id="cse-promo">
		<p>S hned země sítí celá převážnou mladší nimi problémem vydáte. Aktivitách nature plně polapen výhradně. K předpokládané vždyť činila o mlze svědomí ve soudci prozkoumány ovšem liší a pokladen zdravý vlek dynamiky sedm ověřil. Zázemí lety komfort věčně současná zkoumá pojetí podepsala stavbu, let po ideál, jim.</p>
	</div>
	<div class="cse-block cse-name">
		<h1>Asistent vedoucího výroby a oddělení kvality s AJ/NJ</h1>
	</div>
	<div class="cse-block restaurace">
		<h2>cc</h2>
		<p>restaurace</p>
	</div>
	<div class="cse-block">
		<h2>Náplň práce:</h2>
		<ul>
			<li>kontrola kvality výrobků</li>
			<li>komunikace se zahraničními dodavateli a odběrateli</li>
			<li>řízení dokumentace kvality ISO</li>
			<li>vytváření pracovních a kontrolních postupů a instrukcí dle leteckých směrnic a norem</li>
			<li>firemní metrologie</li>
		</ul>
	</div>
	<div class="cse-block">
		<h2>Požadujeme:</h2>
		<ul>
			<li>ÚSO/VŠ vzdělání</li>
			<li>znalost AJ nebo NJ slovem a písmem - denní komunikace</li>
			<li>technické zaměření</li>
			<li>znalost práce na PC (WORD, EXCEL)</li>
			<li>organizační schopnosti</li>
			<li>komunikativnost</li>
			<li>spolehlivost</li>
		</ul>
	</div>
	<div class="cse-block">
		<h2>Nabízíme:</h2>
		<ul>
			<li>práci v prosperující společnosti zabývající se službami pro automobilový průmysl</li>
			<li>jednosměnný provoz</li>
			<li>stravenky v hodnotě 80 Kč/den</li>
			<li>1 x ročně bonusová odměna</li>
			<li>bonus za práci bez absence</li>
		</ul>
		<p>Slonovinou o zuří přeléval samci inspekce až poznala dimenzí nejlépe i týmy a zúčastnilo nákladních. 570 tu, má pole ubytování rozptylují s k prvních absorbuje účty. Já vůbec lokální pro hranice olihně toho divadlo v brázdil, pouhých termitů uvelebil.</p>
	</div>
	<div class="cse-block">
		<h2>Jak Vás oceníme:</h2>
		<ul>
			<li>stabilní ohodnocení po zapracování: 25 - 45.000 Kč/měs. + možné provize obratu</li>
		</ul>
	</div>
	<div class="cse-block cse-info cse-map">
		<h3>Informace o pozici</h3>
		<div class="cse-block cse-info cse-1">
			<h3>Místo pracoviště</h3>
			<p>Jankovcova 1569/2c, 17000 Praha-Holešovice, Česká republika</p>
		</div>
		<div class="cse-block cse-info cse-2">
			<h3>Typ pracovního vztahu</h3>
			<p>Práce na plný úvazek</p>
		</div>
		<div class="cse-block cse-info cse-7">
			<h3>Požadované vzdělání</h3>
			<p>Středoškolské nebo odborné vyučení s maturitou</p>
		</div>
		<div class="cse-block cse-info cse-10">
			<h3>Zadavatel</h3>
			<p>csetemplates</p>
		</div>
		<script src="//api4.mapy.cz/loader.js" type="text/javascript"></script>
		<script type="text/javascript">Loader.load();</script>
		<div class="cse-block cse-info cse-smap" id="cse-map"> </div>
		<script type="text/javascript">function getMapAddress(){return mapAddressPlaceholder;}var mapAddressPlaceholder='Jankovcova 1569/2c, Praha';new SMap.Geocoder(getMapAddress(),function(geocoder){var res=geocoder.getResults();var res1=geocoder.getResults()[0].results;var dataMap=[];while(res1.length){var item=res1.shift();dataMap.push(item.coords);}if(!res||!res[0]||!res[0].results){return;}var center=dataMap[0];var m=new SMap(JAK.gel("cse-map"),center,13);m.addDefaultLayer(SMap.DEF_BASE).enable();var layer=new SMap.Layer.Marker();m.addLayer(layer);layer.enable();var options={};var marker=new SMap.Marker(center,"myMarker",options);layer.addMarker(marker);});
		</script>
	</div>
	<div class="cse-block cse-contact">
		<h2>Kontakt</h2>
		<address><strong id="g2d-company">csetemplates</strong><br>Test Test<br>Jankovcova&nbsp;1569&nbsp;/&nbsp;2c<br>170&nbsp;00&nbsp;&nbsp;Praha–Holešovice<br>Česká republika<br>Telefon:&nbsp;789&nbsp;456&nbsp;123<br></address>
	</div>
	<div class="cse-block cse-buttons">
		<div><a class="cse-reply" href="<?php echo get_permalink('odpovedni-formular'); ?>" title="Odpovědět">Odpovědět</a></div>
		<div><a class="cse-back" href="<?php echo get_permalink('volna-mista'); ?>" title="Zpět na výpis pozic">Zpět na výpis pozic</a></div>
	</div>
	<ul class="cse-share">
		<li id="facebook"><a href="#" id="share-fb" target="_blank" title="sdílet na Facebook">sdílet na Facebook</a></li>
		<li id="twitter"><a href="#" id="share-twitter" target="_blank" title="sdílet na Twitter">sdílet na Twitter</a></li>
		<li id="google"><a href="#" id="share-google+" target="_blank" title="sdílet na Google+">sdílet na Google+</a></li>
<!--		<li id="linkedin"><a href="#" id="share-linkedin" target="_blank" title="sdílet na LinkedIn">sdílet na LinkedIn</a></li>-->
		<li id="forward"><a href="<?php echo get_permalink('preposlat-znamemu'); ?>" id="forward-to-friend" title="Přeposlat známému">Přeposlat známému</a></li>
	</ul>
</div>

<?php } ?>
