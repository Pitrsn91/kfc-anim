<?php
$texts = new CD\Lang();

if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/msie ([2-9]|10)/i', htmlspecialchars($_SERVER['HTTP_USER_AGENT']))) {?>
    <div id="deprecated-browser-message">
        <?= $texts->e('start'); ?>
        <a class="external-link" rel="nofollow" target="_blank" href="https://support.microsoft.com/<?= str_replace('_', '-', strtolower(get_locale())); ?>/help/17621/internet-explorer-downloads">
            <?= $texts->e('link'); ?>
        </a>
        <?= $texts->e('end'); ?>&nbsp;&nbsp;<span id="deprecated-browser-ico-close">&times;</span>
    </div>
    <script src="//presentation.lmc.cz/libs/deprecated-browser/deprecated-browser.min.js<?= CD\GlobalVersion::get();?>"></script>
<?php } ?>
