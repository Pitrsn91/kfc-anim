<?php
$themeUrl = preg_replace( '/http.?:/', '', get_template_directory_uri() );
?>
<meta content="http:<?php echo $themeUrl; ?>/images/share-icons/logo-share-1200x630.png<?php v(); ?>" property="og:image">
<meta content="https:<?php echo $themeUrl; ?>/images/share-icons/logo-share-1200x630.png<?php v(); ?>" property="og:image:secure_url">
<meta content="1200" property="og:image:width">
<meta content="630" property="og:image:height">
<meta content="http:<?php echo $themeUrl; ?>/images/share-icons/logo-share-200x200.png<?php v(); ?>" property="og:image">
<meta content="https:<?php echo $themeUrl; ?>/images/share-icons/logo-share-200x200.png<?php v(); ?>" property="og:image:secure_url">
<meta content="200" property="og:image:width">
<meta content="200" property="og:image:height">
