<?php
if (is_page(CD\ThemeConfig::getDetailPagesIds())) {
	getDetailMeta();
} else {
	echo '<meta name="description" content="Chcete pracovat pro společnost ' .CD\ThemeConfig::getCompanyName() . ' nebo doporučit místo svým známým? Navštivte sekci KARIÉRA, kde jsou všechna aktuální volná pracovní místa.">';
}
