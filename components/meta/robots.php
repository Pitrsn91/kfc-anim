<?php
if (is_page(CD\ThemeConfig::getNoIndexPagesIds()) || is_404() ) {
	echo '<meta name="robots" content="noindex, nofollow">';
} else {
	echo '<meta name="robots" content="index, follow">';
}
