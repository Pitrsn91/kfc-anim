var ANIM_ENGINE = {
    sections : [],
    totalScroll : 0,
    activeSection : 0,
    addSection : function(section){
        console.log(section);
        if(section && typeof section === 'object'){
            this.sections.push(section);
        }
    },
    run : function () {
        this.fakeScroll.init();
        this.scroll.init();
    },
    fakeScroll : {
        object : null,
        init : function(){
            this.create();
            this.setup();
        },
        create : function(){
            this.object = document.createElement('div')
            this.object.className = 'fake-scroll'
            document.body.appendChild(this.object);
        },
        setup : function () {
            var windowHeight = window.innerHeight;
            var countSections = ANIM_ENGINE.sections.length + 1; // i kdyz je jeden frame, potrebujeme scrolovat
            ANIM_ENGINE.totalScroll = windowHeight * countSections;
            this.object.style.height = ANIM_ENGINE.totalScroll + "px";
        }
    },
    scroll : {
        offset : 0,
        percentageValue : 0,
        timer : null,
        init : function (){
            this.timer = requestAnimationFrame(this.updateScroll);

        },
        updateScroll : function () {
            ANIM_ENGINE.scroll.offset = window.pageYOffset || document.documentElement.scrollTop;
            ANIM_ENGINE.scroll.percentageValue = Math.round((ANIM_ENGINE.scroll.offset / window.innerHeight) * 100, 0);
            ANIM_ENGINE.render();
            ANIM_ENGINE.scroll.timer = requestAnimationFrame(ANIM_ENGINE.scroll.updateScroll);
        }
    },
    render : function(){
       var activeFrames = ANIM_ENGINE.animationHandler.selectActiveFrames();
       console.log(activeFrames);
    },
    animationHandler : {
        selectActiveFrames : function(){
            var activeFrames = [];
            var sectionsCount = ANIM_ENGINE.sections.length;

            for (var i = 0; i < sectionsCount; i++){
                var frames = ANIM_ENGINE.sections[i].frames;
                var framesCount = frames.length;

                for(var j = 0; j < framesCount; j++){
                    var frameStart = frames[j].start;
                    var frameEnd = frames[j].end;

                    if(ANIM_ENGINE.scroll.percentageValue >= frameStart && ANIM_ENGINE.scroll.percentageValue <= frameEnd){
                        var frame = frames[j];
                        frame.offset = ANIM_ENGINE.sections[i].offset;
                        frame.speed = ANIM_ENGINE.sections[i].speed;
                        ANIM_ENGINE.animationHandler.animateFrame(frame);
                    }
                }
            }

            return activeFrames;
        },
        animateFrame : function(frame){
            //TODO repair
            var animLength = frame.end - frame.start;
            var step = animLength / 100;
            console.log("step - " + step);
            var percentPositionInFrame = ANIM_ENGINE.scroll.percentageValue / step;
            console.log(ANIM_ENGINE.scroll.percentageValue);

            if(percentPositionInFrame > 100){
                percentPositionInFrame = 100;
            }

            console.log(percentPositionInFrame);

            /*if(isNaN(percentPositionInFrame)){
                percentPositionInFrame = 100;
            }*/

            for(var type in frame.styleStart) {
                console.log(type);

                var styleLength = frame.styleEnd[type] - frame.styleStart[type];

                if(type == "width" || type == "height"){
                    var value = styleLength * (percentPositionInFrame / 100);
                    var suffix = "vh";

                }else if(type == "opacity"){
                    var value = styleLength * percentPositionInFrame / 100;
                    var suffix = "";
                }

                if(value < frame.styleStart[type]){
                    value = frame.styleStart[type];
                }

                if(value > frame.styleEnd[type]){
                    value = frame.styleEnd[type];
                }

                ANIM_ENGINE.animationHandler.animateSingleValue(frame.selector, type, value, suffix);
            }
        },
        animateSingleValue : function(selector, type, value, suffix){
            var options = {};
            options[type] = value + suffix;
            console.log(options);
            $(selector).stop().animate(options, 100, "linear");
        }
    }
};

var section = function(offset, speed) {
    this.offset = offset || 0;
    this.speed = speed || 1;
    this.frames = [];

    this.addFrame = function(elementSelector,styleStart, styleEnd, animPercentStart, animPercentEnd, easing){
        var frame = {
            selector : elementSelector,
            styleStart : styleStart,
            styleEnd : styleEnd,
            start : animPercentStart,
            end : animPercentEnd,
            easing : easing
        };

        this.frames.push(frame);
    };

    this.create = function(){
        return {
            offset : this.offset,
            speed : this.speed,
            frames : this.frames
        }
    }
};

//include section
var section1 = new section();

section1.addFrame(
    '.section-1 .line-1',
    {height: 2},
    {height: 60},
    0,
    20,
    'linear'
);

section1.addFrame(
    '.section-1 .line-2',
    {height: 2},
    {height: 60},
    0,
    20,
    'linear'
);

section1.addFrame(
    '.section-1 .line-3',
    {height: 2},
    {height: 60},
    0,
    20,
    'linear'
);

section1.addFrame(
    '.section-1 .line-4',
    {height: 2},
    {height: 60},
    0,
    20,
    'linear'
);

section1.addFrame(
    '.section-1 .line-5',
    {height: 2},
    {height: 60},
    0,
    20,
    'linear'
);

section1.addFrame(
    '.section-1 .line-1',
    {height: 60},
    {height: 1},
    21,
    40,
    'linear'
);

section1.addFrame(
    '.section-1 .line-2',
    {height: 60},
    {height: 1},
    21,
    40,
    'linear'
);

section1.addFrame(
    '.section-1 .line-3',
    {height: 60},
    {height: 1},
    21,
    40,
    'linear'
);

section1.addFrame(
    '.section-1 .line-4',
    {height: 60},
    {height: 1},
    21,
    40,
    'linear'
);

section1.addFrame(
    '.section-1 .line-5',
    {height: 60},
    {height: 1},
    21,
    40,
    'linear'
);

section1.addFrame(
    '.section-1 .background',
    {opacity: 0},
    {opacity: 1},
    20,
    21,
    'linear'
);

section1.addFrame(
    '.section-1 .background',
    {height: 60, opacity: 1},
    {height: 100, opacity: 1},
    21,
    40,
    'linear'
);

ANIM_ENGINE.addSection(section1.create());


ANIM_ENGINE.run();

