<?php

namespace CD;

/**
 * Class ThemeConfig
 *
 * This class extends ThemeConfigBase. This class stores config for website like company name, keywords etc.
 * There should not be any <b>TODO</b> after config
 *
 * @package CD
 */

class ThemeConfig extends \CD\ThemeConfigBase {
	static protected $companyName = "Název společnosti"; //TODO Název společnosti
	static protected $detailPageIds = array("detail"); //TODO nastavit page id (detail pozice)
	static protected $noIndexPageIds = array("detail", "odpovedni-formular","preposlat-znamemu"); //TODO nastavit page id (detail pozice, formulare pozice)
}
