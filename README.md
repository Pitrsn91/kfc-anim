# KS

## "Flexi" formulář (flexi dot)
- Url pro zobrazení formuláře - `?page=odpovedni-formular&flexi`
- V defaultu se zobrazí flexi dot, který je blokový s nápovědou pomocí hintů
- Zobrazení jiného typu flexi dotu, se děje pomocí přepínače `&flexi=2` (varianty: 2, 3, 4)

## Parametry pro zobrazení variant stránek
- **List**
	- `?page=volna-mista&empty` - prázdný výpis
- **Detail**
	- `?page=detail&invalid` - detail neplatné pozice
- **Formulář**
	- `?page=odpovedni-formular&invalid` - formulář neplatné pozice
	- `?page=odpovedni-formular&reply-sent=1` - success (hurá) obrazovka
	
## Postup pri príprave kariérnych stránok s viacerými dizajnami
Ak niektoré časti kariérnych stránok sú v úplne odlišnom dizajne (napr. rozcestník, pobočka, facebook), tak je 
dobré tomu prispôsobiť aj štruktúru projektu. Odporúčaný postup, s ukážkou a potrebnými krokmi je spísaný 
v confluence: https://confluence.int.lmc.cz/pages/viewpage.action?pageId=70200596


