const gulp = require('gulp');
const del = require('del');
const fs = require("fs");
const readline = require('readline');
const git = require('gulp-git');
const rename = require('gulp-rename');
const watch = require('gulp-watch');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');
const LessAutoprefix = require('less-plugin-autoprefix');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const imageminPngquant = require('imagemin-pngquant');
const replace = require('gulp-replace');
const browserSync = require('browser-sync').create();

/* ################################################## Settings ###################################################### */

const autoprefix = new LessAutoprefix({browsers: ['last 2 versions', 'ie >= 11']});

const color = {
	err: '\x1b[31m%s\x1b[0m',
	warn: '\x1b[33m%s\x1b[0m',
	info: '\x1b[34m%s\x1b[0m'
};

const paths = {
	css: {
		source: ['less/*.less', 'components/**/*.less'],
		target: 'css'
	},
	js: {
		source: ['js/script.js', 'components/**/*.js'],
		target: 'js'
	},
	images: {
		source: 'images/**/*',
		target: 'images'
	}
};

const cssUrlRegex = new RegExp("(url\\([^)]*\\.(?:jpg|jpeg|png|gif|svg|ttf|otf|woff2|woff|eot|webp))(\\?[^)]*)*", 'gmi');

/* ################################################## Globals ####################################################### */

/**
 * Adds 'v' parameter with hash to end of url. If there is query string with another parameters, it is added
 * to the end of new url.
 *
 * @param {string} url, e.g. images/sample.jpg
 * @param {string} hash, e.g. 123456
 * @param {string} params, e.g. ?#iefix
 * @returns {string} e.g. images/sample.jpg?v=123456&#iefix
 */
function addHashToUrl(url, hash, params) {
	return url + '?v=' + hash + (params ? '&' + params.substr(1) : '');
}

/**
 * Generate hash string from current date
 * @returns {string}
 */
function generateHash(){
	let d = new Date();
	return "" + d.getFullYear() + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds();
}

/**
 * Moves files and folders between folders. Rules are:
 * <ul>
 *     <li>All in "images" is saved in "/images/<source name>"</li>
 *     <li>All in "fonts" is saved in "/fonts/<source name>"</li>
 *     <li>"lang.json" is saved in "/_tmp/cd-modules/components/<source name>"</li>
 * </ul>
 * @param {string} name Module name
 * @param {string} from What to move
 * @param {string} to Where to move
 */
function moveFiles(name, from, to) {
	try {
		fs.accessSync(to + '/' + name, fs.constants.F_OK);
		console.log(color.warn, 'Module ' + name + ' already exists');
		return 0;
	} catch (e) {
		try {
			fs.accessSync(`${from}/${name}`, fs.constants.F_OK);
			console.log(color.info, `Module ${name} moved to ${to}/${name}`);
			gulp.src(`${from}/${name}/images/*`).pipe(gulp.dest(`images/${name}`));
			gulp.src(`${from}/${name}/fonts/*`).pipe(gulp.dest(`fonts/${name}`));
			gulp.src(`${from}/${name}/lang.json`).pipe(gulp.dest(`_tmp/cd-modules/components/${name}`));
			return gulp.src([
				`${from}/${name}/**`,
				`!${from}/${name}/images`,
				`!${from}/${name}/images/**`,
				`!${from}/${name}/fonts`,
				`!${from}/${name}/fonts/**`,
				`!${from}/${name}/lang.json`,
				`!${from}/${name}/README.md`
			]).pipe(gulp.dest(`${to}/${name}`));
		} catch (e) {
			console.log(color.err, `Module ${name} doesn't exists!`);
			return 0;
		}
	}
}

/**
 * Clones repository into /tmp
 * @param {function} moveFce Function called if is cloning ok
 */
function getModulesFromRepository(moveFce) {
	console.log('Getting data from repository:');
	del('tmp');
	return git.clone('ssh://git@stash.int.lmc.cz:7999/hra/cd-modules.git', {args: 'tmp'}, function (err) {
		if (err) {
			throw err;
		} else {
			moveFce();
		}
		setTimeout(function () {
			return del('tmp');
		}, 3000);
	});
}

/**
 * Handler functions for errors
 */
const errorHandlers = {
	css: function(err) {
		console.log('css: ');
		err.extract.forEach(function (val) {
			console.log(color.err, val);
		});
		this.emit('end');
	},
	js: function(err) {
		console.log('js: ');
		console.log(color.err, err);
		this.emit('end');
	},
	skip: function(err) {
		this.emit('end');
	}
};

/* ################################################## Tasks ######################################################### */

/**
 * Install script. Gets parts of template from other repositories
 */
gulp.task('default', function () {
	return getModulesFromRepository(function () {
		console.log('Moving modules:');
		moveFiles('src', 'tmp', '_tmp/cd-modules');
		moveFiles('deprecated-browser', 'tmp/components', 'components');
		moveFiles('meta', 'tmp/components', 'components');
	});
});

/**
 * Gets component(s) from repository.
 */
gulp.task('getComponent', function () {
	let rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});

	rl.question('\x1b[7mWhich components (name or names separated by " "):\x1b[0m\n', function (components) {
		if (components !== '') {
			components = components.split(' ');
			getModulesFromRepository(function () {
				components.forEach(function (comp) {
					moveFiles(comp, 'tmp/components', 'components');
				});
			});
		} else {
			console.log(color.err, 'No components given!');
		}
		rl.close();
		process.stdin.destroy();
		return 0;
	});
});

/**
 * Compiles styles with sourcemaps. For development
 */
gulp.task('css:dev', function () {
	return gulp.src(paths.css.source, {base: '.'})
	           .pipe(sourcemaps.init())
	           .pipe(concat('style.min.css'))
	           .pipe(less().on('error', errorHandlers.css))
	           .pipe(sourcemaps.write('.'))
	           .pipe(gulp.dest(paths.css.target))
        		.pipe(browserSync.stream());
});

/**
 * Compiles final minimized styles. For prod
 */
gulp.task('css:build', function () {
	del(paths.css.target + '/style.min.css.map');
	let hash = generateHash();
	return gulp.src(paths.css.source)
	           .pipe(concat('style.min.css'))
	           .pipe(less({plugins: [autoprefix]}).on('error', errorHandlers.css))
	           .pipe(cleanCss())
	           .pipe(replace(cssUrlRegex, (match, url, params) => addHashToUrl(url, hash, params)))
	           .pipe(gulp.dest(paths.css.target))
.pipe(browserSync.stream());
});

/**
 * Compiles vendor styles into one file.
 * @info
 * vendors/vendors.less with "@import (less) ..."
 */
gulp.task('css:vendors', function () {
	let hash = generateHash();
	return gulp.src('vendors/vendors.less')
	           .pipe(less({
		           paths: ['./vendors'],
		           relativeUrls: true
	           }).on('error', errorHandlers.css))
	           .pipe(cleanCss())
	           .pipe(rename({suffix: '.min'}))
	           .pipe(replace(cssUrlRegex, (match, url, params) => addHashToUrl(url, hash, params)))
	           .pipe(gulp.dest('vendors'))
        .pipe(browserSync.stream());
});

/**
 * Compiles javascript with sourcemaps. For developing
 */
gulp.task('js:dev', function () {
	return gulp.src(paths.js.source)
	           .pipe(sourcemaps.init())
	           .pipe(concat('script.min.js'))
	           .pipe(sourcemaps.write('.'))
	           .pipe(gulp.dest(paths.js.target))
        .pipe(browserSync.stream());
});

/**
 * Compiles final minimized javascript. For prod
 */
gulp.task('js:build', function () {
	del(paths.js.target + '/script.min.js.map');
	return gulp.src(paths.js.source)
	           .pipe(concat('script.min.js'))
	           .pipe(uglify().on('error', errorHandlers.js))
	           .pipe(gulp.dest(paths.js.target))
        .pipe(browserSync.stream());
});

/**
 * Compiles js from vendors/vendors.js.
 * @info
 * Use "//.../.../<name>.js"
 */
gulp.task('js:vendors', function () {
	fs.readFile('vendors/vendors.js', 'utf8', function (err, data) {
		if (err) {
			return console.log(color.err, err);
		} else {
			let paths = data.match(/^\/\/\s?(.*\.js);?.*/gm) || [];
			paths.forEach(function (val, index) {
				paths[index] = val.replace(/(\/\/\s?|;.*)/g, '');
			});
			return gulp.src(paths, {cwd: 'vendors'})
			           .pipe(concat('vendors.min.js'))
			           .pipe(uglify().on('error', errorHandlers.js))
			           .pipe(gulp.dest('vendors'))
                .pipe(browserSync.stream());
		}
	});
});

/**
 * Minify jpg, png, gif and svg files from images folder
 */
gulp.task('image:min', function () {
	return gulp.src(paths.images.source)
	           .pipe(imagemin([
		           imageminJpegRecompress({ //jpg
			           loops: 4,
			           min: 60,
			           max: 90,
			           quality: 'high'
		           }),
		           imageminPngquant({ //png
			           speed: 3,
			           quality: 90 //lossy settings
		           }),
		           imagemin.svgo({ //svg
			           plugins: [{
				           removeViewBox: false
			           }, {
				           cleanupIDs: false
			           }, {
				           convertStyleToAttrs: false
			           }]
		           })
	           ], {
		           verbose: true
	           }))
	           .pipe(gulp.dest(paths.images.target));
});

/**
 * Builds css and js for prod
 */
gulp.task('build', ['build:our', 'build:vendors']);

/**
 * Builds css and js of our own creation for prod
 */
gulp.task('build:our', ['css:build', 'js:build', 'image:min']);

/**
 * Builds css and js from vendors for prod
 */
gulp.task('build:vendors', ['css:vendors', 'js:vendors']);

/**
 * Starts watcher over less and js
 */
gulp.task('watch', ['css:dev', 'js:dev'], function () {
	watch(paths.css.source, function () {
		gulp.start(['css:dev']);
	});
	watch(paths.js.source, function () {
		gulp.start(['js:dev']);
	});

    browserSync.init({
        proxy: {
            target: "http://127.0.0.1:8080/edsa-kfc/",
        }

    });
});
