<?php get_header(); ?>

<?php
if ( function_exists( 'get404' ) ) {
	echo get404();
} else {
	echo "<p>" . _e( 'Nothing found for the requested page. Try a search instead?', 'blankslate' ) . "</p>";
	get_search_form();
}
?>

<?php get_footer(); ?>